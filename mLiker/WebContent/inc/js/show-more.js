$(document).ready(function() {
    var showChar = 200;
    var ellipsestext = "...";
    var moretext = "more";
    var lesstext = "less";
    var rate = $("#rate-value").html();
    
    switch (rate) {
    case "9":
    	$("#star_1").attr("src", "inc/pictures/half_star_grey.png");
    	$("#star_2").attr("src", "inc/pictures/full_star_grey.png");
    	$("#star_3").attr("src", "inc/pictures/full_star_grey.png");
    	$("#star_4").attr("src", "inc/pictures/full_star_grey.png");
    	$("#star_5").attr("src", "inc/pictures/full_star_grey.png");
    	break;
    case "8":
    	$("#star_1").attr("src", "inc/pictures/empty_star_grey.png");
    	$("#star_2").attr("src", "inc/pictures/full_star_grey.png");
    	$("#star_3").attr("src", "inc/pictures/full_star_grey.png");
    	$("#star_4").attr("src", "inc/pictures/full_star_grey.png");
    	$("#star_5").attr("src", "inc/pictures/full_star_grey.png");
    	break;
    case "7":
    	$("#star_1").attr("src", "inc/pictures/empty_star_grey.png");
    	$("#star_2").attr("src", "inc/pictures/half_star_grey.png");
    	$("#star_3").attr("src", "inc/pictures/full_star_grey.png");
    	$("#star_4").attr("src", "inc/pictures/full_star_grey.png");
    	$("#star_5").attr("src", "inc/pictures/full_star_grey.png");
    	break;
    case "6":
    	$("#star_1").attr("src", "inc/pictures/empty_star_grey.png");
    	$("#star_2").attr("src", "inc/pictures/empty_star_grey.png");
    	$("#star_3").attr("src", "inc/pictures/full_star_grey.png");
    	$("#star_4").attr("src", "inc/pictures/full_star_grey.png");
    	$("#star_5").attr("src", "inc/pictures/full_star_grey.png");
    	break;
    case "5":
    	$("#star_1").attr("src", "inc/pictures/empty_star_grey.png");
    	$("#star_2").attr("src", "inc/pictures/empty_star_grey.png");
    	$("#star_3").attr("src", "inc/pictures/half_star_grey.png");
    	$("#star_4").attr("src", "inc/pictures/full_star_grey.png");
    	$("#star_5").attr("src", "inc/pictures/full_star_grey.png");
    	break;
    case "4":
    	$("#star_1").attr("src", "inc/pictures/empty_star_grey.png");
    	$("#star_2").attr("src", "inc/pictures/empty_star_grey.png");
    	$("#star_3").attr("src", "inc/pictures/empty_star_grey.png");
    	$("#star_4").attr("src", "inc/pictures/full_star_grey.png");
    	$("#star_5").attr("src", "inc/pictures/full_star_grey.png");
    	break;
    case "3":
    	$("#star_1").attr("src", "inc/pictures/empty_star_grey.png");
    	$("#star_2").attr("src", "inc/pictures/empty_star_grey.png");
    	$("#star_3").attr("src", "inc/pictures/empty_star_grey.png");
    	$("#star_4").attr("src", "inc/pictures/half_star_grey.png");
    	$("#star_5").attr("src", "inc/pictures/full_star_grey.png");
    	break;
    case "2":
    	$("#star_1").attr("src", "inc/pictures/empty_star_grey.png");
    	$("#star_2").attr("src", "inc/pictures/empty_star_grey.png");
    	$("#star_3").attr("src", "inc/pictures/empty_star_grey.png");
    	$("#star_4").attr("src", "inc/pictures/empty_star_grey.png");
    	$("#star_5").attr("src", "inc/pictures/full_star_grey.png");
    	break;
    case "1":
    	$("#star_1").attr("src", "inc/pictures/empty_star_grey.png");
    	$("#star_2").attr("src", "inc/pictures/empty_star_grey.png");
    	$("#star_3").attr("src", "inc/pictures/empty_star_grey.png");
    	$("#star_4").attr("src", "inc/pictures/empty_star_grey.png");
    	$("#star_5").attr("src", "inc/pictures/helf_star_grey.png");
    	break;
    default :
    	$("#star_1").attr("src", "inc/pictures/empty_star.png");
		$("#star_2").attr("src", "inc/pictures/empty_star.png");
		$("#star_3").attr("src", "inc/pictures/empty_star.png");
		$("#star_4").attr("src", "inc/pictures/empty_star.png");
		$("#star_5").attr("src", "inc/pictures/empty_star.png");
    }
    
    $('.more').each(function() {
        var content = $(this).html();
 
        if(content.length > showChar) {
            var c = content.substr(0, showChar);
            var h = content.substr(showChar-1, content.length - showChar);
 
            var html = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span><span class="morecontent" ><span style="display:none;">' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';
 
            $(this).html(html);
        }
 
    });
 
    $(".morelink").click(function(){
        if($(this).hasClass("less")) {
            $(this).removeClass("less");
            $(this).html(moretext);
        } else {
            $(this).addClass("less");
            $(this).html(lesstext);
        }
        $(this).parent().prev().toggle();
        $(this).prev().toggle();
        return false;
    });
    
    $("#star_1").mousemove(function(event) {
    	var parentOffset = $(this).offset();
    	var position = event.pageY - parentOffset.top;
    	var pas;
    	var salt = "";
    	
    	if ($(this).attr("src").indexOf("grey") != -1) {
    		salt = "_grey";
    	}
    	
    	if (position > 24) {
    		$(this).attr("src", "inc/pictures/half_star" + salt + ".png");
    		$("#rate-value").html("9");
    	} else {
    		$(this).attr("src", "inc/pictures/full_star" + salt + ".png");
    		$("#rate-value").html("10");
    	}
    	
    	for (pas = 2; pas <= 5; pas++) {
    		$("#star_" + pas).attr("src", "inc/pictures/full_star" + salt + ".png");
    	}
    	
    });
    
    $("#star_2").mousemove(function(event) {
    	var parentOffset = $(this).offset();
    	var position = event.pageY - parentOffset.top;
    	var pas;
    	var salt = "";
    	
    	if ($(this).attr("src").indexOf("grey") != -1) {
    		salt = "_grey";
    	}
    	
    	if (position > 24) {
    		$(this).attr("src", "inc/pictures/half_star" + salt + ".png");
    		$("#rate-value").html("7");
    	} else {
    		$(this).attr("src", "inc/pictures/full_star" + salt + ".png");
    		$("#rate-value").html("8");
    	}
    	
    	$("#star_1").attr("src", "inc/pictures/empty_star" + salt + ".png");
    	
    	for (pas = 3; pas <= 5; pas++) {
    		$("#star_" + pas).attr("src", "inc/pictures/full_star" + salt + ".png");
    	}
    });
    
    $("#star_3").mousemove(function(event) {
    	var parentOffset = $(this).offset();
    	var position = event.pageY - parentOffset.top;
    	var pas;
    	var salt = "";
    	
    	if ($(this).attr("src").indexOf("grey") != -1) {
    		salt = "_grey";
    	}
    	
    	if (position > 24) {
    		$(this).attr("src", "inc/pictures/half_star" + salt + ".png");
    		$("#rate-value").html("5");
    	} else {
    		$(this).attr("src", "inc/pictures/full_star" + salt + ".png");
    		$("#rate-value").html("6");
    	}
    	
    	for (pas = 1; pas <= 2; pas++) {
    		$("#star_" + pas).attr("src", "inc/pictures/empty_star" + salt + ".png");
    	}
    	
    	for (pas = 4; pas <= 5; pas++) {
    		$("#star_" + pas).attr("src", "inc/pictures/full_star" + salt + ".png");
    	}
    });
    
    $("#star_4").mousemove(function(event) {
    	var parentOffset = $(this).offset();
    	var position = event.pageY - parentOffset.top;
    	var pas;
    	var salt = "";
    	
    	if ($(this).attr("src").indexOf("grey") != -1) {
    		salt = "_grey";
    	}
    	
    	if (position > 24) {
    		$(this).attr("src", "inc/pictures/half_star" + salt + ".png");
    		$("#rate-value").html("3");
    	} else {
    		$(this).attr("src", "inc/pictures/full_star" + salt + ".png");
    		$("#rate-value").html("4");
    	}
    	
    	for (pas = 1; pas <= 3; pas++) {
    		$("#star_" + pas).attr("src", "inc/pictures/empty_star" + salt + ".png");
    	}
    	
    	$("#star_5").attr("src", "inc/pictures/full_star" + salt + ".png");
    	
    });
    
    $("#star_5").mousemove(function(event) {
    	var parentOffset = $(this).offset();
    	var position = event.pageY - parentOffset.top;
    	var pas;
    	var salt = "";
    	
    	if ($(this).attr("src").indexOf("grey") != -1) {
    		salt = "_grey";
    	}
    	
    	if (position > 24) {
    		$(this).attr("src", "inc/pictures/half_star" + salt + ".png");
    		$("#rate-value").html("1");
    	} else {
    		$(this).attr("src", "inc/pictures/full_star" + salt + ".png");
    		$("#rate-value").html("2");
    	}
    	
    	for (pas = 1; pas <= 4; pas++) {
    		$("#star_" + pas).attr("src", "inc/pictures/empty_star" + salt + ".png");
    	}
    });
    
    $(".icon-star").click(function(event) {
    	var rateValue = $("#rate-value").html();
    	$("#rating-form\\:rate-hidden").attr("value", rateValue);
    	
    	updateStars(rateValue);
		$(".hidden_button").click();
    });
});

function updateStars(rate) {
	
	switch (rate) {
    case "9":
    	$("#star_1").attr("src", "inc/pictures/half_star_grey.png");
    	$("#star_2").attr("src", "inc/pictures/full_star_grey.png");
    	$("#star_3").attr("src", "inc/pictures/full_star_grey.png");
    	$("#star_4").attr("src", "inc/pictures/full_star_grey.png");
    	$("#star_5").attr("src", "inc/pictures/full_star_grey.png");
    	break;
    case "8":
    	$("#star_1").attr("src", "inc/pictures/empty_star_grey.png");
    	$("#star_2").attr("src", "inc/pictures/full_star_grey.png");
    	$("#star_3").attr("src", "inc/pictures/full_star_grey.png");
    	$("#star_4").attr("src", "inc/pictures/full_star_grey.png");
    	$("#star_5").attr("src", "inc/pictures/full_star_grey.png");
    	break;
    case "7":
    	$("#star_1").attr("src", "inc/pictures/empty_star_grey.png");
    	$("#star_2").attr("src", "inc/pictures/half_star_grey.png");
    	$("#star_3").attr("src", "inc/pictures/full_star_grey.png");
    	$("#star_4").attr("src", "inc/pictures/full_star_grey.png");
    	$("#star_5").attr("src", "inc/pictures/full_star_grey.png");
    	break;
    case "6":
    	$("#star_1").attr("src", "inc/pictures/empty_star_grey.png");
    	$("#star_2").attr("src", "inc/pictures/empty_star_grey.png");
    	$("#star_3").attr("src", "inc/pictures/full_star_grey.png");
    	$("#star_4").attr("src", "inc/pictures/full_star_grey.png");
    	$("#star_5").attr("src", "inc/pictures/full_star_grey.png");
    	break;
    case "5":
    	$("#star_1").attr("src", "inc/pictures/empty_star_grey.png");
    	$("#star_2").attr("src", "inc/pictures/empty_star_grey.png");
    	$("#star_3").attr("src", "inc/pictures/half_star_grey.png");
    	$("#star_4").attr("src", "inc/pictures/full_star_grey.png");
    	$("#star_5").attr("src", "inc/pictures/full_star_grey.png");
    	break;
    case "4":
    	$("#star_1").attr("src", "inc/pictures/empty_star_grey.png");
    	$("#star_2").attr("src", "inc/pictures/empty_star_grey.png");
    	$("#star_3").attr("src", "inc/pictures/empty_star_grey.png");
    	$("#star_4").attr("src", "inc/pictures/full_star_grey.png");
    	$("#star_5").attr("src", "inc/pictures/full_star_grey.png");
    	break;
    case "3":
    	$("#star_1").attr("src", "inc/pictures/empty_star_grey.png");
    	$("#star_2").attr("src", "inc/pictures/empty_star_grey.png");
    	$("#star_3").attr("src", "inc/pictures/empty_star_grey.png");
    	$("#star_4").attr("src", "inc/pictures/half_star_grey.png");
    	$("#star_5").attr("src", "inc/pictures/full_star_grey.png");
    	break;
    case "2":
    	$("#star_1").attr("src", "inc/pictures/empty_star_grey.png");
    	$("#star_2").attr("src", "inc/pictures/empty_star_grey.png");
    	$("#star_3").attr("src", "inc/pictures/empty_star_grey.png");
    	$("#star_4").attr("src", "inc/pictures/empty_star_grey.png");
    	$("#star_5").attr("src", "inc/pictures/full_star_grey.png");
    	break;
    case "1":
    	$("#star_1").attr("src", "inc/pictures/empty_star_grey.png");
    	$("#star_2").attr("src", "inc/pictures/empty_star_grey.png");
    	$("#star_3").attr("src", "inc/pictures/empty_star_grey.png");
    	$("#star_4").attr("src", "inc/pictures/empty_star_grey.png");
    	$("#star_5").attr("src", "inc/pictures/helf_star_grey.png");
    	break;
    default :
    	$("#star_1").attr("src", "inc/pictures/empty_star.png");
		$("#star_2").attr("src", "inc/pictures/empty_star.png");
		$("#star_3").attr("src", "inc/pictures/empty_star.png");
		$("#star_4").attr("src", "inc/pictures/empty_star.png");
		$("#star_5").attr("src", "inc/pictures/empty_star.png");
    }
}