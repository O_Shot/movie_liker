var requete;

		function insertOrDeleteMovie(idMovie, action) {

			var donnees = $("#checked_" + idMovie);
			var url = "movie?idMovie=" + escape(idMovie) + "&action=" + escape(action);

			if (action == "insert") {
				donnees.attr("src", "inc/pictures/remove.png");
				donnees.attr("onclick", "insertOrDeleteMovie(" +  idMovie + ", \"delete\")");
			} else {
				donnees.attr("src", "inc/pictures/add.png");
				donnees.attr("onclick", "insertOrDeleteMovie(" +  idMovie + ", \"insert\")");
			}
			
			if (window.XMLHttpRequest) {
				requete = new XMLHttpRequest();
			} else if (window.ActiveXObject) {
				requete = new ActiveXObject("Microsoft.XMLHTTP");
			}

			requete.open("GET", url, true);
			requete.onreadystatechange = majIHM;
			requete.send(null);
		}

		function majIHM() {
			var message = "";

			if (requete.readyState == 4) {
				if (requete.status == 200) {
					// exploitation des données de la réponse
					var messageTag = requete.responseXML
							.getElementsByTagName("message")[0];
					message = messageTag.childNodes[0].nodeValue;
					mdiv = document.getElementById("validationMessage");
					if (message == "invalide") {
						mdiv.innerHTML = "<img src='images/invalide.gif'>";
					} else {
						mdiv.innerHTML = "<img src='images/valide.gif'>";
					}
				}
			}
		}