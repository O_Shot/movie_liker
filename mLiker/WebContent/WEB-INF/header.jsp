
<nav class="navbar navbar-default">
	<div class="container-fluid">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed"
				data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href='<c:url value="/home"/>'>MyLiker</a>
		</div>

		<div class="collapse navbar-collapse"
			id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav">
				<li class="active"><a href="#">Home </a></li>
				<li><a href="/mLiker/movie">My Movies</a></li>
				<li><a href="#">My Finder</a></li>
				
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="true">My Space
						<span class="caret"></span>
					</a>
					<ul class="dropdown-menu" role="menu">
						<li><a href="#">Last watched</a></li>
						<li><a href="#">My criteria</a></li>
						<li><a href="#">My awards</a></li>
						<li class="divider"></li>
						<li><a href="#">My profile</a></li>
						<li class="divider"></li>
						<li><a href="#">My friends</a></li>
					</ul></li>
			</ul>
			<form class="navbar-form navbar-left" role="search" method="post" action="movie">
				<div class="form-group">
					<input type="text" class="form-control" name="title" placeholder="Search">
				</div>
				<button type="submit" class="btn btn-default">Submit</button>
			</form>
			<ul class="nav navbar-nav navbar-right">
				<c:choose>
					<c:when test="${sessionScope.logged == true}">
						<li><a><c:out value="Logged as ${sessionScope.sessionUtilisateur.pseudo }"/></a></li>
						<li><a href='<c:url value="/disconnect"/>'>Log out</a></li>
					</c:when>
					<c:otherwise>
						<li><a href='<c:url value="/connect"/>'>Sign in</a></li>
						<li><a href='<c:url value="/register"/>'>Register</a></li>
					</c:otherwise>
				</c:choose>
			</ul>
		</div>
	</div>
</nav>