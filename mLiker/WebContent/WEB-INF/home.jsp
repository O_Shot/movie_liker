<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Homepage</title>
		<link type="text/css" rel="stylesheet" href="inc/css/bootstrap.min.css" />
		<link type="text/css" rel="stylesheet" href="inc/css/style.css" />
		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	    <script src="inc/js/jquery-1.11.3.min.js"></script>
	    <!-- Include all compiled plugins (below), or include individual files as needed -->
	    <script src="inc/js/bootstrap.min.js"></script>
	</head>
	<body>
		<c:import url="header.jsp"></c:import>
		<form class="form-horizontal">
		  <fieldset>
		    <legend>Menu principal</legend>
		    <div class="form-group">
		      <label for="textArea" class="col-lg-2 control-label">Textarea</label>
		      <div class="col-lg-10">
		        <textarea class="form-control" rows="3" id="textArea"></textarea>
		        <span class="help-block">A longer block of help text that breaks onto a new line and may extend beyond one line.</span>
		      </div>
		    </div>
		    <div class="form-group">
		      <label class="col-lg-2 control-label">Radios</label>
		      <div class="col-lg-10">
		        <div class="radio">
		          <label>
		            <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1">
		            Option one is this
		          </label>
		        </div>
		        <div class="radio">
		          <label>
		            <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
		            Option two can be something else
		          </label>
		        </div>
		      </div>
		    </div>
		    <div class="form-group">
		      <label for="select" class="col-lg-2 control-label">Selects</label>
		      <div class="col-lg-10">
		        <select class="form-control" id="select">
		          <option>1</option>
		          <option>2</option>
		          <option>3</option>
		          <option>4</option>
		          <option>5</option>
		        </select>
		        <br>
		        <select multiple class="form-control">
		          <option>1</option>
		          <option>2</option>
		          <option>3</option>
		          <option>4</option>
		          <option>5</option>
		        </select>
		      </div>
		    </div>
		    <div class="form-group">
		      <div class="col-lg-10 col-lg-offset-2">
		        <button type="reset" class="btn btn-default">Cancel</button>
		        <button type="submit" class="btn btn-primary">Submit</button>
		      </div>
		    </div>
		  </fieldset>
		</form>
		
		
	</body>
</html>