<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Movie Liker - Registration</title>
	<link type="text/css" rel="stylesheet" href="inc/css/bootstrap.min.css" />
	<link type="text/css" rel="stylesheet" href="inc/css/style.css" />
</head>
<body>
	<c:import url="header.jsp"></c:import>
	<form class="form-horizontal" method="post" action="register">
		<fieldset>
			<legend>Registration</legend>
			
			<div class="form-group">
				<label for="pseudo" class="col-lg-2 control-label">Pseudo</label>
				<div class="col-lg-2">
					<input type="text" class="form-control" name="pseudo"
						placeholder="Pseudo">
					<c:out value="${errors.pseudo }"/>
				</div>
			</div>
			
			<div class="form-group">
				<label for="email" class="col-lg-2 control-label">Email</label>
				<div class="col-lg-2">
					<input type="text" class="form-control" name="email"
						placeholder="Email">
					<c:out value="${errors.email }"/>
				</div>
			</div>
			
			<div class="form-group">
				<label for="password" class="col-lg-2 control-label">Password</label>
				<div class="col-lg-2">
					<input type="password" class="form-control" name="password"
						placeholder="Password">
				</div>
			</div>
			
			<div class="form-group">
				<label for="password_check" class="col-lg-2 control-label">Confirm
					your password</label>
				<div class="col-lg-2">
					<input type="password" class="form-control" name="password_check"
						placeholder="Password">
					<c:out value="${errors.password }"/>
					<div class="checkbox">
						<label> <input type="checkbox"> Remember me
						</label>
					</div>
				</div>
			</div>
			<c:out value="${success}" />
			<c:out value="${errors.exist }"/>
			<div class="form-group">
				<div class="col-lg-10 col-lg-offset-2">
					<button type="reset" class="btn btn-default">Cancel</button>
					<button type="submit" class="btn btn-primary">Submit</button>
				</div>
			</div>
		</fieldset>
	</form>
</body>
</html>