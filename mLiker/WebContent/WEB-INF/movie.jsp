<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>mLiker - Titanic</title>
	<link type="text/css" rel="stylesheet" href="inc/css/bootstrap.min.css" />
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="inc/js/jquery-1.11.3.min.js"></script>
	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script src="inc/js/bootstrap.min.js"></script>
</head>
<body>
	<c:import url="header.jsp"></c:import>
	<div class="panel panel-default">
		<div class="panel-heading">Results</div>
		<div class="panel-body">
			<table class="table table-striped table-hover ">
				<thead>
					<tr>
						<th>#</th>
						<th>Movie title</th>
						<th>Released date</th>
						<th>IMDb Rate</th>
						<th>Genre</th>
						<th>Watched</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${movies }" var="movie">
						<tr>
							<td><c:out value="${movie.id }"/></td>
							<td><c:out value="${movie.title }"/></td>
							<td><c:out value="${movie.year }"/></td>
							<td><c:out value="${movie.rate }"/></td>
							<td><c:out value="${movie.genre }"/></td>
							<td><input type="checkbox" name="watched"/></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			<c:out value="${errors['title'] }"></c:out>
		</div>
	</div>
</body>
</html>