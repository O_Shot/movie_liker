<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Infos - <c:out value="${movie.title }"></c:out></title>
	<link type="text/css" rel="stylesheet" href="inc/css/bootstrap.min.css" />
	<link type="text/css" rel="stylesheet" href="inc/css/style.css" />
</head>
<body>
	<c:import url="header.jsp"></c:import>
	<div class="head_movie">
		<div class="col-lg-1"></div>
		<div class="col-lg-6">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h3 class="panel-title">Poster</h3>
				</div>
				<div class="panel-body"><img src="${movie.poster }"/></div>
			</div>
		</div>
		<div class="col-lg-1"></div>
		<div class="col-lg-3">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h3 class="panel-title">Information</h3>
				</div>
				<div class="panel-body">Hello bitches ;)</div>
			</div>
		</div>
		<div class="col-lg-1"></div>
	</div>
	<div>
		<table class="table table-striped table-hover ">
			<thead>
				<tr>
					<th>#</th>
					<th>Movie title</th>
					<th>Released date</th>
					<th>IMDB Id</th>
					<th>IMDb Rate</th>
					<th>Genre</th>
					<th>Votes</th>
					<th>Duration</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td><c:out value="${movie.id }" /></td>
					<td><c:out value="${movie.title }" /></td>
					<td><c:out value="${movie.year }" /></td>
					<td><c:out value="${movie.imdbId }"></c:out></td>
					<td><c:out value="${movie.rate }" /></td>
					<td><c:out value="${movie.genre }" /></td>
					<td><c:out value="${movie.votes }" /></td>
					<td><fmt:formatNumber value="${movie.duration div 60}" maxFractionDigits="0"/><c:out value=" h ${movie.duration mod 60 } m"/></td>
				</tr>
			</tbody>
		</table>
		<br/>
		<c:out value="Director : ${movie.director.idArtist} ; ${movie.director.firstName} ${movie.director.lastName }"></c:out>
		<br/>
		<c:out value="Acteurs : "/><br/>
		<table class="table table-striped table-hover ">
			<thead>
				<tr>
					<th>Actor id</th>
					<th>First Name</th>
					<th>Last Name</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach items="${movie.actors }" var="actor">
					<tr>
						<td><c:out value="${actor.idArtist }" /></td>
						<td><c:out value="${actor.firstName }" /></td>
						<td><c:out value="${actor.lastName }" /></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
</body>
</html>