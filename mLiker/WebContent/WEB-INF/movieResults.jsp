<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>mLiker - Titanic</title>
<link type="text/css" rel="stylesheet" href="inc/css/bootstrap.min.css" />
<link type="text/css" rel="stylesheet" href="inc/css/style.css" />
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="inc/js/jquery-1.11.3.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="inc/js/bootstrap.min.js"></script>
</head>
<body>
	<c:import url="header.jsp"></c:import>
	<div class="panel panel-default">
		<div class="panel-heading">Results</div>
		<div class="panel-body">
			<table class="table table-striped table-hover ">
				<thead>
					<tr>
						<th>#</th>
						<th>Movie title</th>
						<th>Released date</th>
						<th>IMDb Rate</th>
						<th>Genre</th>
						<c:if test="${sessionScope.logged == true}">
							<th>Watched</th>
						</c:if>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${movies }" var="movie">
						<tr>
							<td><c:out value="${movie.id }" /></td>
							<td><a href='<c:url value="/display?idMovie=${movie.id }"/>'><c:out value="${movie.title }" /></a></td>
							<td><c:out value="${movie.year }" /></td>
							<td><c:out value="${movie.rate }" /></td>
							<td><c:out value="${movie.genre }" /></td>
							<c:if test="${sessionScope.logged == true}">
								<td><c:choose>
									<c:when test="${movie.watched }">
										<img id="checked_${movie.id }" class="icon_style"  src="inc/pictures/remove.png" name="watched"
											onclick='insertOrDeleteMovie(${movie.id}, "delete");' />
									</c:when>
									<c:otherwise>
										<img id="checked_${movie.id }" class="icon_style" src="inc/pictures/add.png" name="watched"
											onclick='insertOrDeleteMovie(${movie.id}, "insert");' />
									</c:otherwise>
								</c:choose></td>
							</c:if>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			<c:out value="${errors['title'] }"></c:out>
		</div>
	</div>

	<script type="text/javascript">
		var requete;

		function insertOrDeleteMovie(idMovie, action) {

			var donnees = $("#checked_" + idMovie);
			var url = "movie?idMovie=" + escape(idMovie) + "&action=" + escape(action);

			if (action == "insert") {
				donnees.attr("src", "inc/pictures/remove.png");
				donnees.attr("onclick", "insertOrDeleteMovie(" +  idMovie + ", \"delete\")");
			} else {
				donnees.attr("src", "inc/pictures/add.png");
				donnees.attr("onclick", "insertOrDeleteMovie(" +  idMovie + ", \"insert\")");
			}
			
			if (window.XMLHttpRequest) {
				requete = new XMLHttpRequest();
			} else if (window.ActiveXObject) {
				requete = new ActiveXObject("Microsoft.XMLHTTP");
			}

			requete.open("GET", url, true);
			requete.onreadystatechange = majIHM;
			requete.send(null);
		}

		function majIHM() {
			var message = "";

			if (requete.readyState == 4) {
				if (requete.status == 200) {
					// exploitation des données de la réponse
					var messageTag = requete.responseXML
							.getElementsByTagName("message")[0];
					message = messageTag.childNodes[0].nodeValue;
					mdiv = document.getElementById("validationMessage");
					if (message == "invalide") {
						mdiv.innerHTML = "<img src='images/invalide.gif'>";
					} else {
						mdiv.innerHTML = "<img src='images/valide.gif'>";
					}
				}
			}
		}
	</script>
</body>
</html>