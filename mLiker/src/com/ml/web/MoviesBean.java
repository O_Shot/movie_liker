package com.ml.web;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import com.ml.model.Movies;
import com.ml.service.MoviesService;

@ManagedBean
@SessionScoped
public class MoviesBean implements Serializable {

    private static final long serialVersionUID = 8150756503956053844L;

    @ManagedProperty( "#{moviesService}" )
    private MoviesService     moviesService;

    private List<Movies>      moviesList;
    private Movies            movie;

    private String            title;

    public MoviesBean() {

    }

    public void find() {

        List<Object[]> results = moviesService.findByTitle( title );
        List<Movies> movies = new ArrayList<Movies>();
        Movies movie;

        for ( Object[] result : results ) {
            movie = new Movies( (String) result[1], (String) result[2], (String) result[3], "" );
            movie.setMovieid( (Integer) result[0] );

            movies.add( movie );
        }

        setMoviesList( movies );

        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect( "movieresults.xhtml" );
        } catch ( Exception e ) {
            e.getMessage();
        }
    }

    public List<Movies> getMoviesList() {
        return moviesList;
    }

    public void setMoviesList( List<Movies> moviesList ) {
        this.moviesList = moviesList;
    }

    public Movies getMovie() {
        return movie;
    }

    public void setMovie( Movies movie ) {
        this.movie = movie;
    }

    public MoviesService getMoviesService() {
        return moviesService;
    }

    public void setMoviesService( MoviesService moviesService ) {
        this.moviesService = moviesService;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle( String title ) {
        this.title = title;
    }

}
