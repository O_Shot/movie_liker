package com.ml.web;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

import com.ml.model.Actors;
import com.ml.model.Directors;
import com.ml.model.Genres;
import com.ml.model.Movies;
import com.ml.model.Plots;
import com.ml.model.Producers;
import com.ml.model.Ratings;
import com.ml.model.Releasedates;
import com.ml.model.Runningtimes;
import com.ml.model.Writers;
import com.ml.service.MoviesService;

@ManagedBean
@RequestScoped
public class DisplayBean implements Serializable {

    private static final long serialVersionUID = 8150756503956053844L;

    @ManagedProperty( "#{moviesService}" )
    private MoviesService     moviesService;

    private Integer           idMovie;
    private Integer           userRate;
    private String            cast;
    private Movies            movie;
    private List<Actors>      actors;
    private Directors         director;
    private Writers           writer;
    private List<Producers>   producers;
    private List<Producers>   executives;
    private Plots             plot;
    private Ratings           ratings;
    private List<Genres>      genres;
    private Runningtimes      runningTimes;
    private Releasedates      releaseDates;

    public DisplayBean() {

    }

    public void init() {

    }

    public void display() {
        int count = 1;
        String movieid;
        UsersBean user = (UsersBean) FacesContext.getCurrentInstance().getExternalContext()
                .getSessionMap().get( "usersBean" );

        Map<String, String> args = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();

        if ( args.containsKey( "rating-form:idMovie" ) ) {
            movieid = args.get( "rating-form:idMovie" );
        } else {
            movieid = args.get( "idMovie" );
        }

        setIdMovie( Integer.parseInt( movieid ) );
        setMovie( moviesService.getById( idMovie ) );
        setCast( moviesService.getCast( this.movie.getImdbid() ) );
        setActors( moviesService.getActors( idMovie ) );
        setDirector( moviesService.getDirector( idMovie ) );
        setWriter( moviesService.getWriter( idMovie ) );
        setProducers( moviesService.getProducers( idMovie ) );
        setExecutives( moviesService.getExecutive( idMovie ) );
        setPlot( moviesService.getPlot( idMovie ) );
        setRatings( moviesService.getRatings( idMovie ) );
        setGenre( moviesService.getGenres( idMovie ) );
        setRunningTimes( moviesService.getRunningTimes( idMovie ) );
        setReleaseDates( moviesService.getReleaseDates( idMovie ) );

        if ( !user.getPseudo().equals( "nop" ) ) {
            setUserRate( moviesService.getUsersRate( idMovie, user.getIdUser() ) );
        } else {
            setUserRate( 0 );
        }

        if ( this.userRate == null ) {
            if ( args.containsKey( "rating-form:rate-hidden" ) ) {
                setUserRate( Integer.parseInt( args.get( "rating-form:rate-hidden" ) ) );
            } else {
                setUserRate( 0 );
            }
        }

        this.director.setName( formatName( this.director.getName() ) );
        this.writer.setName( formatName( this.writer.getName() ) );
        for ( Actors actor : actors ) {
            actor.setName( formatName( actor.getName() ) );

            if ( count < actors.size() ) {
                actor.setName( actor.getName() + ", " );
            }
            count++;
        }

        count = 1;

        for ( Producers producer : producers ) {
            producer.setName( formatName( producer.getName() ) );

            if ( count < producers.size() ) {
                producer.setName( producer.getName() + ", " );
            }
            count++;
        }

        count = 1;

        for ( Producers executive : executives ) {
            executive.setName( formatName( executive.getName() ) );

            if ( count < executives.size() ) {
                executive.setName( executive.getName() + ", " );
            }
            count++;
        }

        count = 1;

        for ( Genres genre : genres ) {
            genre.getId().setGenre( formatName( genre.getId().getGenre() ) );

            if ( count < genres.size() ) {
                genre.getId().setGenre( genre.getId().getGenre() + ", " );
            }
            count++;
        }
    }

    public void saveRating() {
        Map<String, String> parameterMap = FacesContext.getCurrentInstance().getExternalContext()
                .getRequestParameterMap();
        UsersBean user = (UsersBean) FacesContext.getCurrentInstance().getExternalContext()
                .getSessionMap().get( "usersBean" );

        setUserRate( Integer.parseInt( parameterMap.get( "rating-form:rate-hidden" ) ) );
        Integer idMovie = Integer.parseInt( parameterMap.get( "rating-form:idMovie" ) );

        if ( !user.getPseudo().equals( "nop" ) ) {
            moviesService.insert( idMovie, user.getIdUser(), this.userRate );
        }
    }

    private String formatName( String name ) {

        String formatName = name;
        String firstName;
        String lastName;

        if ( name.indexOf( "(" ) != -1 ) {
            formatName = name.substring( 0, name.indexOf( "(" ) );
        }

        if ( name.indexOf( "," ) != -1 ) {
            firstName = formatName.substring( formatName.indexOf( "," ) + 1 ).trim();
            lastName = formatName.substring( 0, formatName.indexOf( "," ) ).trim();

            return firstName + " " + lastName;
        }

        return formatName;
    }

    public Movies getMovie() {
        return movie;
    }

    public void setMovie( Movies movie ) {
        this.movie = movie;
    }

    public MoviesService getMoviesService() {
        return moviesService;
    }

    public void setMoviesService( MoviesService moviesService ) {
        this.moviesService = moviesService;
    }

    public List<Actors> getActors() {
        return actors;
    }

    public void setActors( List<Actors> actors ) {
        this.actors = actors;
    }

    public Directors getDirector() {
        return director;
    }

    public void setDirector( Directors director ) {
        this.director = director;
    }

    public Plots getPlot() {
        return plot;
    }

    public void setPlot( Plots plot ) {
        this.plot = plot;
    }

    public Ratings getRatings() {
        return ratings;
    }

    public void setRatings( Ratings ratings ) {
        this.ratings = ratings;
    }

    public List<Genres> getGenre() {
        return genres;
    }

    public void setGenre( List<Genres> genres ) {
        this.genres = genres;
    }

    public Integer getIdMovie() {
        return idMovie;
    }

    public void setIdMovie( Integer idMovie ) {
        this.idMovie = idMovie;
    }

    public List<Genres> getGenres() {
        return genres;
    }

    public void setGenres( List<Genres> genres ) {
        this.genres = genres;
    }

    public Runningtimes getRunningTimes() {
        return runningTimes;
    }

    public void setRunningTimes( Runningtimes runningTimes ) {
        this.runningTimes = runningTimes;
    }

    public Writers getWriter() {
        return writer;
    }

    public void setWriter( Writers writer ) {
        this.writer = writer;
    }

    public List<Producers> getProducers() {
        return producers;
    }

    public void setProducers( List<Producers> producers ) {
        this.producers = producers;
    }

    public List<Producers> getExecutives() {
        return executives;
    }

    public void setExecutives( List<Producers> executives ) {
        this.executives = executives;
    }

    public String getCast() {
        return cast;
    }

    public void setCast( String cast ) {
        this.cast = cast;
    }

    public Releasedates getReleaseDates() {
        return releaseDates;
    }

    public void setReleaseDates( Releasedates releaseDates ) {
        this.releaseDates = releaseDates;
    }

    public Integer getUserRate() {
        return userRate;
    }

    public void setUserRate( Integer userRate ) {
        this.userRate = userRate;
    }
}
