package com.ml.web;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import com.ml.model.UsersMovie;
import com.ml.service.Movies2UsersService;

@ManagedBean
@SessionScoped
public class Movies2UsersBean implements Serializable {

    private static final long   serialVersionUID = 8150756503656053844L;

    @ManagedProperty( "#{movies2UsersService}" )
    private Movies2UsersService movies2UsersService;

    private List<UsersMovie>    usersMovies;

    public Movies2UsersBean() {

    }

    public void display() {

        UsersBean user = (UsersBean) FacesContext.getCurrentInstance().getExternalContext()
                .getSessionMap().get( "usersBean" );
        Integer idUser = user.getIdUser();

        if ( idUser != null ) {
            setUsersMovies( movies2UsersService.getUsersMovies( idUser ) );
        }
    }

    public Movies2UsersService getMovies2UsersService() {
        return movies2UsersService;
    }

    public void setMovies2UsersService( Movies2UsersService movies2UsersService ) {
        this.movies2UsersService = movies2UsersService;
    }

    public List<UsersMovie> getUsersMovies() {
        return usersMovies;
    }

    public void setUsersMovies( List<UsersMovie> usersMovies ) {
        this.usersMovies = usersMovies;
    }
}
