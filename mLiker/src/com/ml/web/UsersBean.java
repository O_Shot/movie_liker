package com.ml.web;

import java.io.IOException;
import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import com.ml.service.UsersService;

@ManagedBean
@SessionScoped
public class UsersBean implements Serializable {

    private static final long serialVersionUID = 8150756503656053844L;

    @ManagedProperty( "#{usersService}" )
    private UsersService      usersService;

    private Integer           idUser;
    private String            email;
    private String            password;
    private String            pseudo;

    public UsersBean() {

    }

    @PostConstruct
    public void init() {
        setPseudo( "nop" );
    }

    public void connect() {

        setPseudo( usersService.existingUser( email, password ) );
        setIdUser( usersService.getIdUser( email ) );

        if ( !pseudo.equals( "nop" ) ) {
            try {
                FacesContext.getCurrentInstance().getExternalContext().redirect( "home.xhtml" );
            } catch ( Exception e ) {
                e.getMessage();
            }
        }
    }

    public void disconnect() {
        setIdUser( null );
        setEmail( "" );
        setPassword( "" );
        setPseudo( "nop" );

        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect( "home.xhtml" );
        } catch ( IOException e ) {
            e.getMessage();
        }
    }

    public void register() {

        usersService.addUser( email, password, pseudo );
    }

    public String getEmail() {
        return email;
    }

    public void setEmail( String email ) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword( String password ) {
        this.password = password;
    }

    public String getPseudo() {
        return pseudo;
    }

    public void setPseudo( String pseudo ) {
        this.pseudo = pseudo;
    }

    public UsersService getUsersService() {
        return usersService;
    }

    public void setUsersService( UsersService usersService ) {
        this.usersService = usersService;
    }

    @Override
    public String toString() {
        return "Password : " + password + " ; Pseudo : " + pseudo + " ; Email : " + email;
    }

    public Integer getIdUser() {
        return idUser;
    }

    public void setIdUser( Integer idUser ) {
        this.idUser = idUser;
    }
}
