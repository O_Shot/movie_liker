package com.ml.web;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

import com.ml.service.UsersService;

@ManagedBean
@RequestScoped
public class RegisterBean implements Serializable {

    private static final long serialVersionUID = 8150736503656053844L;

    @ManagedProperty( "#{usersService}" )
    private UsersService      usersService;

    private String            email;
    private String            password;
    private String            pseudo;

    public RegisterBean() {

    }

    public void register() {

        usersService.addUser( email, password, pseudo );

        try {
            FacesContext.getCurrentInstance().getExternalContext().redirect( "home.xhtml" );
        } catch ( Exception e ) {
            e.getMessage();
        }
    }

    public String getEmail() {
        return email;
    }

    public void setEmail( String email ) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword( String password ) {
        this.password = password;
    }

    public String getPseudo() {
        return pseudo;
    }

    public void setPseudo( String pseudo ) {
        this.pseudo = pseudo;
    }

    public UsersService getUsersService() {
        return usersService;
    }

    public void setUsersService( UsersService usersService ) {
        this.usersService = usersService;
    }

}
