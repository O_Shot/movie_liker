package com.ml.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ml.dao.DAOFactory;
import com.ml.dao.UtilisateurDao;
import com.ml.dao.UtilisateurDaoImpl;

public class LogFilter extends HttpServlet implements Filter {

    private static final long  serialVersionUID = 7394462841785577641L;
    public static final String ATT_LOGGED       = "logged";
    public static final String ATT_REGISTER     = "register";
    public static final String ATT_SESSION_USER = "sessionUtilisateur";
    public static final String CONF_DAO_FACTORY = "daofactory";
    public static final String COOKIE_CONNECT   = "cookieConnect";

    @Override
    public void init( FilterConfig arg0 ) throws ServletException {
        // TODO Auto-generated method stub

    }

    @Override
    public void doFilter( ServletRequest req, ServletResponse res, FilterChain chain )
            throws IOException, ServletException {

        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;

        String pseudo = getCookieValue( request, COOKIE_CONNECT );
        DAOFactory daofactory = DAOFactory.getInstance();
        UtilisateurDao userDao = new UtilisateurDaoImpl( daofactory );

        HttpSession session = request.getSession();

        if ( session.getAttribute( ATT_LOGGED ) == null ) {
            if ( pseudo != null ) {
                session.setAttribute( ATT_SESSION_USER, userDao.trouverPseudo( pseudo ) );
                session.setAttribute( ATT_LOGGED, true );
            } else {
                session.setAttribute( ATT_LOGGED, "Sign in" );
            }
        }

        chain.doFilter( request, response );
    }

    @Override
    public void destroy() {
        // TODO Auto-generated method stub

    }

    /**
     * Méthode utilitaire gérant la récupération de la valeur d'un cookie donné
     * depuis la requête HTTP.
     */
    private static String getCookieValue( HttpServletRequest request, String nom ) {
        Cookie[] cookies = request.getCookies();
        if ( cookies != null ) {
            for ( Cookie cookie : cookies ) {
                if ( cookie != null && nom.equals( cookie.getName() ) ) {
                    return cookie.getValue();
                }
            }
        }
        return null;
    }

}
