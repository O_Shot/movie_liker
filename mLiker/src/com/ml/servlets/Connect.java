package com.ml.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ml.beans.Utilisateur;
import com.ml.dao.DAOFactory;
import com.ml.dao.UtilisateurDao;
import com.ml.forms.ConnectionForm;

public class Connect extends HttpServlet {

    private static final long  serialVersionUID       = -7133126377075234689L;
    public static final String CONF_DAO_FACTORY       = "daofactory";
    public static final String ATT_SESSION_USER       = "sessionUtilisateur";
    public static final String ATT_USER               = "utilisateur";
    public static final String ATT_FORM               = "form";
    public static final String ATT_SUCCESS            = "success";
    public static final String ATT_ERRORS             = "errors";
    public static final String ATT_LOGGED             = "logged";
    public static final String ATT_REMEMBER           = "remember";
    public static final String COOKIE_CONNECT         = "cookieConnect";
    public static final int    COOKIE_MAX_AGE         = 60 * 60 * 24 * 365;
    public static final String VIEW                   = "/WEB-INF/connect.jsp";
    public static final String VIEW_CONNECTE          = "/WEB-INF/home.jsp";
    public static final String VIEW_REDIRECT          = "/connect";
    public static final String VIEW_REDIRECT_CONNECTE = "/home";

    private UtilisateurDao     utilisateurDao;

    public void init() throws ServletException {
        /* Récupération d'une instance de notre DAO Utilisateur */
        this.utilisateurDao = ( (DAOFactory) getServletContext().getAttribute( CONF_DAO_FACTORY ) ).getUtilisateurDao();
    }

    public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {

        this.getServletContext().getRequestDispatcher( VIEW ).forward( request, response );
    }

    public void doPost( HttpServletRequest request, HttpServletResponse response )
            throws ServletException, IOException {

        String path = "/mLiker";
        /* Préparation de l'objet formulaire */
        ConnectionForm form = new ConnectionForm( utilisateurDao );
        /* Traitement de la requête et récupération du bean en résultant */
        Utilisateur utilisateur = form.connecterUtilisateur( request );
        /* Récupération de la session depuis la requête */
        HttpSession session = request.getSession();

        /*
         * Si aucune erreur de validation n'a eu lieu, alors ajout du bean
         * Utilisateur à la session, sinon suppression du bean de la session.
         */
        if ( form.getErreurs().isEmpty() && utilisateur != null ) {
            session.setAttribute( ATT_SESSION_USER, utilisateur );
            session.setAttribute( ATT_LOGGED, true );
            request.setAttribute( ATT_SUCCESS, "Connection succeeded." );
            path += VIEW_REDIRECT_CONNECTE;
        } else {
            session.setAttribute( ATT_SESSION_USER, null );
            session.setAttribute( ATT_LOGGED, false );
            request.setAttribute( ATT_SUCCESS, "Connection failed." );
            path += VIEW_REDIRECT;
        }

        /* Si et seulement si la case du formulaire est cochée */
        if ( request.getParameter( ATT_REMEMBER ) != null ) {
            setCookie( response, COOKIE_CONNECT, utilisateur.getPseudo(), COOKIE_MAX_AGE );
        } else {
            setCookie( response, COOKIE_CONNECT, "", 0 );
        }

        request.setAttribute( ATT_ERRORS, form.getErreurs() );
        request.setAttribute( ATT_FORM, form );
        request.setAttribute( ATT_USER, utilisateur );

        response.sendRedirect( path );
    }

    /*
     * Méthode utilitaire gérant la création d'un cookie et son ajout à la
     * réponse HTTP.
     */
    private static void setCookie( HttpServletResponse response, String nom, String valeur, int maxAge ) {
        Cookie cookie = new Cookie( nom, valeur );
        cookie.setMaxAge( maxAge );
        response.addCookie( cookie );
    }
}
