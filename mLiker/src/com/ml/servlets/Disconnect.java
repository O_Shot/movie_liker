package com.ml.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class Disconnect extends HttpServlet {

    private static final long  serialVersionUID = -2436341611395959177L;
    public static final String COOKIE_CONNECT   = "cookieConnect";
    public static final String VIEW_HOME        = "/mLiker/home";

    public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {
        /* Récupération et destruction de la session en cours */
        HttpSession session = request.getSession();
        session.invalidate();

        setCookie( response, COOKIE_CONNECT, "", 0 );

        /* Redirection vers le Site du Zéro ! */
        response.sendRedirect( VIEW_HOME );
    }

    /*
     * Méthode utilitaire gérant la création d'un cookie et son ajout à la
     * réponse HTTP.
     */
    private static void setCookie( HttpServletResponse response, String nom, String valeur, int maxAge ) {
        Cookie cookie = new Cookie( nom, valeur );
        cookie.setMaxAge( maxAge );
        response.addCookie( cookie );
    }
}