package com.ml.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ml.dao.DAOFactory;
import com.ml.dao.UtilisateurDao;
import com.ml.forms.RegisterForm;

public class Register extends HttpServlet {

    private static final long  serialVersionUID = -7133126377075234689L;
    public static final String CONF_DAO_FACTORY = "daofactory";
    public static final String ATT_SESSION_USER = "sessionUtilisateur";
    public static final String ATT_USER         = "utilisateur";
    public static final String ATT_FORM         = "form";
    public static final String ATT_SUCCESS      = "success";
    public static final String ATT_ERRORS       = "errors";
    public static final String ATT_LOGGED       = "logged";
    public static final String VIEW             = "/WEB-INF/register.jsp";
    public static final String VIEW_HOME        = "/WEB-INF/home.jsp";

    private UtilisateurDao     utilisateurDao;

    public void init() throws ServletException {
        /* Récupération d'une instance de notre DAO Utilisateur */
        this.utilisateurDao = ( (DAOFactory) getServletContext().getAttribute( CONF_DAO_FACTORY ) ).getUtilisateurDao();
    }

    public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {

        this.getServletContext().getRequestDispatcher( VIEW ).forward( request, response );
    }

    public void doPost( HttpServletRequest request, HttpServletResponse response )
            throws ServletException, IOException {

        String path = "";
        /* Préparation de l'objet formulaire */
        RegisterForm form = new RegisterForm( utilisateurDao );
        /* Traitement de la requête et récupération du bean en résultant */
        form.registerUtilisateur( request );

        if ( form.getErrors().isEmpty() ) {
            request.setAttribute( ATT_SUCCESS, "Connection succeeded." );
            path = VIEW_HOME;
        } else {
            request.setAttribute( ATT_SUCCESS, "Connection failed." );
            path = VIEW;
        }

        request.setAttribute( ATT_ERRORS, form.getErrors() );

        this.getServletContext().getRequestDispatcher( path ).forward( request, response );
    }
}
