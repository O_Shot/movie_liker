package com.ml.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class Home extends HttpServlet {

    private static final long  serialVersionUID = -7133126377075234689L;
    public static final String ATT_LOGGED       = "logged";
    public static final String VUE_CONNECTE     = "/WEB-INF/home.jsp";

    public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {

        HttpSession session = request.getSession();

        if ( session.getAttribute( ATT_LOGGED ) == null ) {
            session.setAttribute( ATT_LOGGED, "Sign in" );
        }

        this.getServletContext().getRequestDispatcher( VUE_CONNECTE ).forward( request, response );
    }
}
