package com.ml.servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.ml.beans.Movie;
import com.ml.dao.DAOFactory;
import com.ml.dao.MovieDao;
import com.ml.forms.MovieForm;

public class MovieInfo extends HttpServlet {

    private static final long  serialVersionUID   = -7133126377075234689L;
    public static final String CONF_DAO_FACTORY   = "daofactory";
    public static final String ATT_LOGGED         = "logged";
    public static final String ATT_MOVIES         = "movies";
    public static final String ATT_ERRORS         = "errors";
    public static final String ATT_ACTION         = "action";
    public static final String PARAM_TITLE        = "title";
    public static final String VIEW_MOVIE_RESULTS = "movieResults.jsp";

    private MovieDao           movieDao;

    public void init() throws ServletException {
        /* Récupération d'une instance de notre DAO Utilisateur */
        this.movieDao = ( (DAOFactory) getServletContext().getAttribute( CONF_DAO_FACTORY ) ).getMovieDao();
    }

    public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {

        String path = "/WEB-INF/";
        HttpSession session = request.getSession();

        MovieForm form = new MovieForm( movieDao );

        if ( request.getParameter( ATT_ACTION ).equals( "insert" ) ) {
            form.insertMovie( request, session );
            path += VIEW_MOVIE_RESULTS;
        } else if ( request.getParameter( ATT_ACTION ).equals( "delete" ) ) {
            form.deleteMovie( request, session );
            path += VIEW_MOVIE_RESULTS;
        }

        this.getServletContext().getRequestDispatcher( path ).forward( request, response );
    }

    public void doPost( HttpServletRequest request, HttpServletResponse response )
            throws ServletException, IOException {

        String path = "/WEB-INF/" + VIEW_MOVIE_RESULTS;
        HttpSession session = request.getSession();

        MovieForm form = new MovieForm( movieDao );
        ArrayList<Movie> movies = form.findMovies( request, session );

        if ( form.getErreurs().isEmpty() ) {
            request.setAttribute( ATT_MOVIES, movies );
        } else {
            request.setAttribute( ATT_ERRORS, form.getErreurs() );
        }

        this.getServletContext().getRequestDispatcher( path ).forward( request, response );
    }
}
