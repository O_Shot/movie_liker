package com.ml.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ml.beans.Movie;
import com.ml.dao.DAOFactory;
import com.ml.dao.MovieDao;
import com.ml.forms.MovieForm;

public class MoviePage extends HttpServlet {

    private static final long  serialVersionUID = -7133126377075234689L;
    public static final String CONF_DAO_FACTORY = "daofactory";
    public static final String ATT_MOVIE        = "movie";
    public static final String ATT_ERRORS       = "errors";
    public static final String PARAM_TITLE      = "title";
    public static final String VIEW_MOVIE_INFO  = "/WEB-INF/movieInfo.jsp";

    private MovieDao           movieDao;

    public void init() throws ServletException {
        /* Récupération d'une instance de notre DAO Utilisateur */
        this.movieDao = ( (DAOFactory) getServletContext().getAttribute( CONF_DAO_FACTORY ) ).getMovieDao();
    }

    public void doGet( HttpServletRequest request, HttpServletResponse response ) throws ServletException, IOException {

        MovieForm form = new MovieForm( movieDao );
        Movie movie = form.displayMovie( request );

        if ( form.getErreurs().isEmpty() ) {
            request.setAttribute( ATT_MOVIE, movie );
        } else {
            request.setAttribute( ATT_ERRORS, form.getErreurs() );
        }

        this.getServletContext().getRequestDispatcher( VIEW_MOVIE_INFO ).forward( request, response );
    }

    public void doPost( HttpServletRequest request, HttpServletResponse response )
            throws ServletException, IOException {

        this.getServletContext().getRequestDispatcher( VIEW_MOVIE_INFO ).forward( request, response );
    }
}
