package com.ml.service;

public interface UsersService {

    boolean existingMail( String email );

    String existingUser( String email, String password );

    Integer getIdUser( String email );

    void addUser( String email, String password, String pseudo );

}
