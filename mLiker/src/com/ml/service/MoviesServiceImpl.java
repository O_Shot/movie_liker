package com.ml.service;

import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ml.model.Actors;
import com.ml.model.Directors;
import com.ml.model.Genres;
import com.ml.model.Movies;
import com.ml.model.Movies2users;
import com.ml.model.Movies2usersId;
import com.ml.model.Plots;
import com.ml.model.Producers;
import com.ml.model.Ratings;
import com.ml.model.Releasedates;
import com.ml.model.Runningtimes;
import com.ml.model.Writers;

@Service( "moviesService" )
@Transactional
public class MoviesServiceImpl implements MoviesService {

    private static String  HQL_SELECT_BY_TITLE  = "SELECT M.movieid, M.title, M.year, M.imdbid, R.id.rank, R.id.votes"
                                                        + " FROM Movies M, Ratings R"
                                                        + " WHERE LOWER(title) LIKE :title"
                                                        + " AND M.movieid = R.id.movieid "
                                                        + " GROUP BY M.movieid"
                                                        + " ORDER BY R.id.votes DESC";

    private static String  HQL_GET_CAST         = "SELECT P.cast FROM Posters P WHERE P.imdbid = :imdbid";

    private static String  HQL_GET_ACTORS       = "SELECT A FROM Actors A, Movies2actors M "
                                                        + "WHERE M.id.movieid = :movieid "
                                                        + "AND A.actorid = M.id.actorid";

    private static String  HQL_GET_DIRECTOR     = "SELECT D FROM Directors D, Movies2directors M "
                                                        + "WHERE M.id.movieid = :movieid "
                                                        + "AND D.directorid = M.id.directorid";

    private static String  HQL_GET_WRITER       = "SELECT W FROM Writers W, Movies2writers M "
                                                        + "WHERE M.id.movieid = :movieid "
                                                        + "AND W.writerid = M.id.writerid";

    private static String  HQL_GET_PRODUCERS    = "SELECT P FROM Producers P, Movies2producers M "
                                                        + "WHERE M.id.movieid = :movieid "
                                                        + "AND P.producerid = M.id.producerid "
                                                        + "AND M.id.addition = '(producer)'";

    private static String  HQL_GET_EXECUTIVES   = "SELECT P FROM Producers P, Movies2producers M "
                                                        + "WHERE M.id.movieid = :movieid "
                                                        + "AND P.producerid = M.id.producerid "
                                                        + "AND M.id.addition = '(executive producer)'";

    private static String  HQL_GET_PLOT         = "FROM Plots WHERE id.movieid = :movieid";

    private static String  HQL_GET_RATINGS      = "FROM Ratings WHERE id.movieid = :movieid";

    private static String  HQL_GET_GENRE        = "FROM Genres WHERE id.movieid = :movieid";

    private static String  HQL_GET_DURATION     = "FROM Runningtimes WHERE id.movieid = :movieid";

    private static String  HQL_GET_RELEASE_DATE = "FROM Releasedates WHERE id.movieid = :movieid"
                                                        + " AND id.country = 'USA'";

    private static String  HQL_DELETE_USER_RATE = "DELETE FROM Movies2users "
                                                        + "WHERE movieid = :movieid "
                                                        + "AND userid = :userid";

    private static String  HQL_GET_USER_RATE    = "SELECT id.rate FROM Movies2users "
                                                        + "WHERE id.movieid = :movieid "
                                                        + "AND id.userid = :userid";

    @Autowired
    private SessionFactory sessionFactory;

    @SuppressWarnings( "unchecked" )
    @Override
    public List<Object[]> findByTitle( String title ) {

        return (List<Object[]>) sessionFactory.getCurrentSession().createQuery( HQL_SELECT_BY_TITLE )
                .setParameter( "title", "%" + title + "%" )
                .setMaxResults( 5 )
                .list();
    }

    @Override
    public Movies getById( Integer idMovie ) {

        return (Movies) sessionFactory.getCurrentSession().get( Movies.class, idMovie );
    }

    @Override
    public String getCast( String imdbId ) {

        return (String) sessionFactory.getCurrentSession()
                .createQuery( HQL_GET_CAST )
                .setParameter( "imdbid", imdbId )
                .uniqueResult();
    }

    @SuppressWarnings( "unchecked" )
    @Override
    public List<Actors> getActors( Integer idMovie ) {

        return sessionFactory.getCurrentSession()
                .createQuery( HQL_GET_ACTORS )
                .setParameter( "movieid", idMovie )
                .list();
    }

    @Override
    public Directors getDirector( Integer idMovie ) {

        return (Directors) sessionFactory.getCurrentSession()
                .createQuery( HQL_GET_DIRECTOR )
                .setParameter( "movieid", idMovie )
                .uniqueResult();

    }

    @Override
    public Writers getWriter( Integer idMovie ) {

        return (Writers) sessionFactory.getCurrentSession()
                .createQuery( HQL_GET_WRITER )
                .setParameter( "movieid", idMovie )
                .uniqueResult();
    }

    @SuppressWarnings( "unchecked" )
    @Override
    public List<Producers> getProducers( Integer idMovie ) {

        return sessionFactory.getCurrentSession()
                .createQuery( HQL_GET_PRODUCERS )
                .setParameter( "movieid", idMovie )
                .list();
    }

    @SuppressWarnings( "unchecked" )
    @Override
    public List<Producers> getExecutive( Integer idMovie ) {

        return sessionFactory.getCurrentSession()
                .createQuery( HQL_GET_EXECUTIVES )
                .setParameter( "movieid", idMovie )
                .list();
    }

    @Override
    public Plots getPlot( Integer idMovie ) {

        return (Plots) sessionFactory.getCurrentSession()
                .createQuery( HQL_GET_PLOT )
                .setParameter( "movieid", idMovie )
                .uniqueResult();
    }

    @Override
    public Ratings getRatings( Integer idMovie ) {

        return (Ratings) sessionFactory.getCurrentSession()
                .createQuery( HQL_GET_RATINGS )
                .setParameter( "movieid", idMovie )
                .uniqueResult();
    }

    @SuppressWarnings( "unchecked" )
    @Override
    public List<Genres> getGenres( Integer idMovie ) {

        return sessionFactory.getCurrentSession()
                .createQuery( HQL_GET_GENRE )
                .setParameter( "movieid", idMovie )
                .list();
    }

    @Override
    public Runningtimes getRunningTimes( Integer idMovie ) {

        return (Runningtimes) sessionFactory.getCurrentSession()
                .createQuery( HQL_GET_DURATION )
                .setParameter( "movieid", idMovie )
                .uniqueResult();
    }

    @Override
    public Releasedates getReleaseDates( Integer idMovie ) {

        return (Releasedates) sessionFactory.getCurrentSession()
                .createQuery( HQL_GET_RELEASE_DATE )
                .setParameter( "movieid", idMovie )
                .setMaxResults( 1 )
                .uniqueResult();
    }

    @Override
    public void insert( Integer idMovie, Integer idUser, Integer rate ) {

        Movies2usersId id = new Movies2usersId( idMovie, idUser, rate );
        Movies2users movies2users = new Movies2users( id );

        sessionFactory.getCurrentSession()
                .createSQLQuery( HQL_DELETE_USER_RATE )
                .setInteger( "movieid", idMovie )
                .setInteger( "userid", idUser ).executeUpdate();

        sessionFactory.getCurrentSession().saveOrUpdate( movies2users );

    }

    @Override
    public void delete( Integer idMovie, Integer idUser, Integer rate ) {

        Movies2usersId id = new Movies2usersId( idMovie, idUser, rate );
        Movies2users movies2users = new Movies2users( id );

        sessionFactory.getCurrentSession().delete( movies2users );
    }

    @Override
    public Integer getUsersRate( Integer idMovie, Integer idUser ) {

        return (Integer) sessionFactory.getCurrentSession().createQuery( HQL_GET_USER_RATE )
                .setParameter( "movieid", idMovie )
                .setParameter( "userid", idUser )
                .uniqueResult();
    }

}
