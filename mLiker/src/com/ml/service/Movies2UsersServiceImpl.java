package com.ml.service;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ml.model.Movies;
import com.ml.model.UsersMovie;

@Service( "movies2UsersService" )
@Transactional
public class Movies2UsersServiceImpl implements Movies2UsersService {

    private static final String HQL_GET_USERS_MOVIES = "SELECT M, U.id.rate FROM Movies M, Movies2users U "
                                                             + "WHERE M.movieid = U.id.movieid "
                                                             + "AND U.id.userid = :userid";

    @Autowired
    private SessionFactory      sessionFactory;

    @Override
    public List<UsersMovie> getUsersMovies( Integer idUser ) {

        List<UsersMovie> usersMovies = new ArrayList<UsersMovie>();

        @SuppressWarnings( "unchecked" )
        List<Object[]> moviesInfos = sessionFactory.getCurrentSession()
                .createQuery( HQL_GET_USERS_MOVIES )
                .setParameter( "userid", idUser )
                .list();

        for ( Object[] moviesInfo : moviesInfos ) {
            usersMovies.add( new UsersMovie( (Movies) moviesInfo[0], (Integer) moviesInfo[1] ) );
        }

        return usersMovies;
    }

}
