package com.ml.service;

import java.util.List;

import com.ml.model.UsersMovie;

public interface Movies2UsersService {

    List<UsersMovie> getUsersMovies( Integer idUser );
}
