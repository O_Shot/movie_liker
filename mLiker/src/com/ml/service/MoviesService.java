package com.ml.service;

import java.util.List;

import com.ml.model.Actors;
import com.ml.model.Directors;
import com.ml.model.Genres;
import com.ml.model.Movies;
import com.ml.model.Plots;
import com.ml.model.Producers;
import com.ml.model.Ratings;
import com.ml.model.Releasedates;
import com.ml.model.Runningtimes;
import com.ml.model.Writers;

public interface MoviesService {

    List<Object[]> findByTitle( String title );

    Movies getById( Integer idMovie );

    String getCast( String imdbId );

    List<Actors> getActors( Integer idMovie );

    Directors getDirector( Integer idMovie );

    Writers getWriter( Integer idMovie );

    List<Producers> getProducers( Integer idMovie );

    List<Producers> getExecutive( Integer idMovie );

    Plots getPlot( Integer idMovie );

    Ratings getRatings( Integer idMovie );

    List<Genres> getGenres( Integer idMovie );

    Runningtimes getRunningTimes( Integer idMovie );

    Releasedates getReleaseDates( Integer idMovie );

    void insert( Integer idMovie, Integer idUser, Integer rate );

    void delete( Integer idMovie, Integer idUser, Integer rate );

    Integer getUsersRate( Integer idMovie, Integer idUser );
}
