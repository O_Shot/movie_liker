package com.ml.service;

import java.security.MessageDigest;
import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.ml.model.Users;

@Service( "usersService" )
@Transactional
public class UsersServiceImpl implements UsersService {

    private static final String HQL_GET_USER  = "FROM Users WHERE email = :email AND password = :password";
    private static final String HQL_GET_EMAIL = "FROM Users WHERE email = :email";
    private static final String HQL_GET_ID    = "SELECT id FROM Users WHERE email = :email";

    @Autowired
    private SessionFactory      sessionFactory;

    @Override
    public boolean existingMail( String email ) {

        @SuppressWarnings( "unchecked" )
        List<Users> users = sessionFactory.getCurrentSession().createQuery( HQL_GET_EMAIL )
                .setParameter( "email", email )
                .list();

        return !users.isEmpty();
    }

    @Override
    public String existingUser( String email, String password ) {

        String pseudo = "nop";
        @SuppressWarnings( "unchecked" )
        List<Users> users = sessionFactory.getCurrentSession().createQuery( HQL_GET_USER )
                .setParameter( "email", email )
                .setParameter( "password", encryptPwd( password ) )
                .list();

        if ( !users.isEmpty() ) {
            pseudo = users.get( 0 ).getPseudo();
        }

        return pseudo;
    }

    @Override
    public Integer getIdUser( String email ) {

        return (Integer) sessionFactory.getCurrentSession().createQuery( HQL_GET_ID )
                .setParameter( "email", email )
                .uniqueResult();
    }

    @Override
    public void addUser( String email, String password, String pseudo ) {

        Users user = new Users( email, pseudo, encryptPwd( password ) );

        sessionFactory.getCurrentSession().save( user );
    }

    public static String encryptPwd( String pwd ) {

        String salt = "EY@!6ye9";

        return encrypt( encrypt( pwd ) + salt );
    }

    public static String encrypt( String pwd ) {
        MessageDigest d = null;
        byte[] encrypted = null;

        try {
            d = MessageDigest.getInstance( "SHA-1" );
            d.reset();
            d.update( pwd.getBytes() );
            encrypted = d.digest();
        } catch ( Exception e ) {
            System.out.println( e.getMessage() );
        }

        return byteArrayToHexString( encrypted );
    }

    public static String byteArrayToHexString( byte[] b ) {
        String result = "";

        if ( b != null ) {
            for ( int i = 0; i < b.length; i++ ) {
                result += Integer.toString( ( b[i] & 0xff ) + 0x100, 16 ).substring( 1 );
            }
        } else {
            result = null;
        }

        return result;
    }

}
