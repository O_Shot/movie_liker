package com.ml.forms;

import java.security.MessageDigest;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.ml.beans.Utilisateur;
import com.ml.dao.UtilisateurDao;

public final class RegisterForm {

    private static final String PARAM_EMAIL    = "email";
    private static final String PARAM_PASSWORD = "password";
    private static final String PARAM_CHECK    = "password_check";
    private static final String PARAM_PSEUDO   = "pseudo";
    private static final String PARAM_ERROR    = "exist";

    private UtilisateurDao      utilisateurDao;
    private Map<String, String> errors         = new HashMap<String, String>();

    public Map<String, String> getErrors() {
        return this.errors;
    }

    public RegisterForm( UtilisateurDao utilisateurDao ) {
        this.utilisateurDao = utilisateurDao;
    }

    public void registerUtilisateur( HttpServletRequest request ) {

        Utilisateur utilisateur = new Utilisateur();

        String email = getValeurChamp( request, PARAM_EMAIL );
        String password = getValeurChamp( request, PARAM_PASSWORD );
        String check = getValeurChamp( request, PARAM_CHECK );
        String pseudo = getValeurChamp( request, PARAM_PSEUDO );

        try {
            validationEmail( email );
        } catch ( Exception e ) {
            setError( PARAM_EMAIL, e.getMessage() );
        }
        utilisateur.setEmail( email );

        try {
            validationPassword( password, check );
        } catch ( Exception e ) {
            setError( PARAM_PASSWORD, e.getMessage() );
        }
        utilisateur.setPassword( encryptPwd( password ) );

        try {
            validationPseudo( pseudo );
        } catch ( Exception e ) {
            setError( PARAM_PSEUDO, e.getMessage() );
        }
        utilisateur.setPseudo( request.getParameter( PARAM_PSEUDO ) );

        if ( errors.isEmpty() ) {
            try {
                newUser( utilisateur );
            } catch ( Exception e ) {
                setError( PARAM_ERROR, e.getMessage() );
            }
        }

    }

    /**
     * Valide l'adresse email saisie.
     */
    private void validationEmail( String email ) throws Exception {
        if ( email != null ) {
            if ( !email.matches( "([^.@]+)(\\.[^.@]+)*@([^.@]+\\.)+([^.@]+)" ) ) {
                throw new Exception( "Merci de saisir une adresse mail valide." );
            } else if ( email.length() > 100 ) {
                throw new Exception( "L'adresse mail ne doit pas contenir plus de 100 caractères" );
            }
        } else {
            throw new Exception( "Merci de saisir une adresse mail" );
        }

    }

    /**
     * Valide le mot de passe saisi.
     */
    private void validationPassword( String password, String check ) throws Exception {
        if ( password != null ) {
            if ( password.length() < 3 ) {
                throw new Exception( "Le mot de passe doit contenir au moins 3 caractères." );
            } else if ( password.length() > 50 ) {
                throw new Exception( "Le mot de passe ne doit pas contenir plus de 50 caractères." );
            } else if ( !password.equals( check ) ) {
                throw new Exception( "Les deux mots de passe ne sont pas identiques." );
            }
        } else {
            throw new Exception( "Merci de saisir votre mot de passe." );
        }
    }

    /**
     * Valide le pseudo
     */

    private void validationPseudo( String pseudo ) throws Exception {
        if ( pseudo != null ) {
            if ( pseudo.length() < 2 ) {
                throw new Exception( "Le pseudo doit contenir au moins 2 caractères." );
            } else if ( pseudo.length() > 50 ) {
                throw new Exception( "Le pseudo ne doit pas contenir plus de 20 caractères." );
            }
        } else {
            throw new Exception( "Merci de saisir votre pseudo" );
        }
    }

    /**
     * 
     * Vérifie l'existence d'un utilisateur dans la base
     */

    private void newUser( Utilisateur utilisateur ) throws Exception {

        if ( utilisateurDao.trouverMail( utilisateur.getEmail() ) != null ) {
            throw new Exception( "L'adresse email saisie est déjà associée à un compte" );
        } else if ( utilisateurDao.trouverPseudo( utilisateur.getPseudo() ) != null ) {
            throw new Exception( "Le pseudo saisi existe déjà" );
        } else {
            utilisateurDao.creer( utilisateur );
        }
    }

    /*
     * Ajoute un message correspondant au champ spécifié à la map des erreurs.
     */
    private void setError( String field, String message ) {
        errors.put( field, message );
    }

    /*
     * Méthode utilitaire qui retourne null si un champ est vide, et son contenu
     * sinon.
     */
    private static String getValeurChamp( HttpServletRequest request, String nomChamp ) {
        String valeur = request.getParameter( nomChamp );
        if ( valeur == null || valeur.trim().length() == 0 ) {
            return null;
        } else {
            return valeur;
        }
    }

    public static String encryptPwd( String pwd ) {

        String salt = "EY@!6ye9";

        return encrypt( encrypt( pwd ) + salt );
    }

    public static String encrypt( String pwd ) {
        MessageDigest d = null;
        byte[] encrypted = null;

        try {
            d = MessageDigest.getInstance( "SHA-1" );
            d.reset();
            d.update( pwd.getBytes() );
            encrypted = d.digest();
        } catch ( Exception e ) {
            System.out.println( e.getMessage() );
        }

        return byteArrayToHexString( encrypted );
    }

    public static String byteArrayToHexString( byte[] b ) {
        String result = "";

        if ( b != null ) {
            for ( int i = 0; i < b.length; i++ ) {
                result += Integer.toString( ( b[i] & 0xff ) + 0x100, 16 ).substring( 1 );
            }
        } else {
            result = null;
        }

        return result;
    }
}
