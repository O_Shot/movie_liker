package com.ml.forms;

import java.security.MessageDigest;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import com.ml.beans.Utilisateur;
import com.ml.dao.UtilisateurDao;

public final class ConnectionForm {
    private static final String CHAMP_EMAIL = "email";
    private static final String CHAMP_PASS  = "password";

    private String              resultat;
    private Map<String, String> erreurs     = new HashMap<String, String>();

    private UtilisateurDao      utilisateurDao;

    public ConnectionForm( UtilisateurDao utilisateurDao ) {
        this.utilisateurDao = utilisateurDao;
    }

    public String getResultat() {
        return resultat;
    }

    public Map<String, String> getErreurs() {
        return erreurs;
    }

    public Utilisateur connecterUtilisateur( HttpServletRequest request ) {
        /* Récupération des champs du formulaire */
        String email = getValeurChamp( request, CHAMP_EMAIL );
        String password = getValeurChamp( request, CHAMP_PASS );

        Utilisateur utilisateur = new Utilisateur();

        /* Validation du champ email. */
        try {
            validationEmail( email );
        } catch ( Exception e ) {
            setErreur( CHAMP_EMAIL, e.getMessage() );
        }

        /* Validation du champ mot de passe. */
        try {
            validationPassword( password );
        } catch ( Exception e ) {
            setErreur( CHAMP_PASS, e.getMessage() );
        }

        /* Initialisation du résultat global de la validation. */
        if ( erreurs.isEmpty() ) {
            utilisateur = this.utilisateurDao.trouver( email, encryptPwd(
                    password ) );

        } else {
            utilisateur = null;
        }

        return utilisateur;
    }

    /**
     * Valide l'adresse email saisie.
     */
    private void validationEmail( String email ) throws Exception {
        if ( email != null && !email.matches( "([^.@]+)(\\.[^.@]+)*@([^.@]+\\.)+([^.@]+)" ) ) {
            throw new Exception( "Merci de saisir une adresse mail valide." );
        }
    }

    /**
     * Valide le mot de passe saisi.
     */
    private void validationPassword( String password ) throws Exception {
        if ( password != null ) {
            if ( password.length() < 3 ) {
                throw new Exception( "Le mot de passe doit contenir au moins 3 caractères." );
            }
        } else {
            throw new Exception( "Merci de saisir votre mot de passe." );
        }
    }

    /*
     * Ajoute un message correspondant au champ spécifié à la map des erreurs.
     */
    private void setErreur( String champ, String message ) {
        erreurs.put( champ, message );
    }

    /*
     * Méthode utilitaire qui retourne null si un champ est vide, et son contenu
     * sinon.
     */
    private static String getValeurChamp( HttpServletRequest request, String nomChamp ) {
        String valeur = request.getParameter( nomChamp );
        if ( valeur == null || valeur.trim().length() == 0 ) {
            return null;
        } else {
            return valeur;
        }
    }

    public static String encryptPwd( String pwd ) {

        String salt = "EY@!6ye9";

        return encrypt( encrypt( pwd ) + salt );
    }

    public static String encrypt( String pwd ) {
        MessageDigest d = null;
        byte[] encrypted = null;

        try {
            d = MessageDigest.getInstance( "SHA-1" );
            d.reset();
            d.update( pwd.getBytes() );
            encrypted = d.digest();
        } catch ( Exception e ) {
            System.out.println( e.getMessage() );
        }

        return byteArrayToHexString( encrypted );
    }

    public static String byteArrayToHexString( byte[] b ) {
        String result = "";

        if ( b != null ) {
            for ( int i = 0; i < b.length; i++ ) {
                result += Integer.toString( ( b[i] & 0xff ) + 0x100, 16 ).substring( 1 );
            }
        } else {
            result = null;
        }

        return result;
    }
}