package com.ml.forms;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.ml.beans.Movie;
import com.ml.beans.Utilisateur;
import com.ml.dao.MovieDao;

public class MovieForm {

    private static final String CHAMP_TITLE      = "title";
    private static final String CHAMP_MOVIE_ID   = "idMovie";
    public static final String  ATT_SESSION_USER = "sessionUtilisateur";

    private String              resultat;
    private Map<String, String> erreurs          = new HashMap<String, String>();
    private MovieDao            movieDao;

    public MovieForm( MovieDao movieDao ) {
        this.movieDao = movieDao;
    }

    public String getResultat() {
        return resultat;
    }

    public Map<String, String> getErreurs() {
        return erreurs;
    }

    public Movie displayMovie( HttpServletRequest request ) {

        Movie movie = null;
        long idMovie = Long.parseLong( getValeurChamp( request, CHAMP_MOVIE_ID ) );

        try {
            movie = movieDao.display( idMovie );
        } catch ( Exception e ) {
            setErreur( CHAMP_MOVIE_ID, e.getMessage() );
        }

        return movie;
    }

    public void insertMovie( HttpServletRequest request, HttpSession session ) {

        Utilisateur utilisateur = (Utilisateur) session.getAttribute( ATT_SESSION_USER );
        ArrayList<Long> moviesList = utilisateur.getMoviesList();
        long idMovie = Long.parseLong( getValeurChamp( request, CHAMP_MOVIE_ID ) );
        long idUser = utilisateur.getId();

        try {
            movieDao.insert( idMovie, idUser );
            moviesList.add( idMovie );
            utilisateur.setMoviesList( moviesList );
        } catch ( Exception e ) {
            setErreur( CHAMP_MOVIE_ID, e.getMessage() );
        }
    }

    public void deleteMovie( HttpServletRequest request, HttpSession session ) {

        Utilisateur utilisateur = (Utilisateur) session.getAttribute( ATT_SESSION_USER );
        ArrayList<Long> moviesList = utilisateur.getMoviesList();
        long idMovie = Long.parseLong( getValeurChamp( request, CHAMP_MOVIE_ID ) );
        long idUser = utilisateur.getId();

        try {
            movieDao.delete( idMovie, idUser );

            if ( moviesList.contains( idMovie ) ) {
                moviesList.remove( idMovie );
                utilisateur.setMoviesList( moviesList );
            }
        } catch ( Exception e ) {
            setErreur( CHAMP_MOVIE_ID, e.getMessage() );
        }

    }

    public ArrayList<Movie> findMovies( HttpServletRequest request, HttpSession session ) {
        /* Récupération des champs du formulaire */
        String title = getValeurChamp( request, CHAMP_TITLE );

        ArrayList<Movie> movies = new ArrayList<Movie>();

        /* Validation du champ email. */
        try {
            validationTitle( title );
        } catch ( Exception e ) {
            setErreur( CHAMP_TITLE, e.getMessage() );
        }

        /* Initialisation du résultat global de la validation. */
        if ( erreurs.isEmpty() ) {
            movies = movieDao.trouver( title, (Utilisateur) session.getAttribute( ATT_SESSION_USER ) );
        } else {
            movies = null;
        }

        return movies;
    }

    /*
     * Ajoute un message correspondant au champ spécifié à la map des erreurs.
     */
    private void setErreur( String champ, String message ) {
        erreurs.put( champ, message );
    }

    private void validationTitle( String title ) throws Exception {
        // TODO Auto-generated method stub
        if ( title != null ) {
            if ( title.length() > 50 ) {
                throw new Exception( "Your title should not exceed 50 characters" );
            }
        } else {
            throw new Exception( "Please enter a title" );
        }
    }

    /*
     * Méthode utilitaire qui retourne null si un champ est vide, et son contenu
     * sinon.
     */
    private static String getValeurChamp( HttpServletRequest request, String nomChamp ) {
        String valeur = request.getParameter( nomChamp );
        if ( valeur == null || valeur.trim().length() == 0 ) {
            return null;
        } else {
            return valeur;
        }
    }
}
