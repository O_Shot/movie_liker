package com.ml.dao;

import com.ml.beans.Utilisateur;

public interface UtilisateurDao {

    void creer( Utilisateur utilisateur ) throws DAOException;

    Utilisateur trouverPseudo( String pseudo ) throws DAOException;

    Utilisateur trouverMail( String email ) throws DAOException;

    Utilisateur trouver( String email, String password ) throws DAOException;

}