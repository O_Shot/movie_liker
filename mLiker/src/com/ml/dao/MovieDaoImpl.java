package com.ml.dao;

import static com.ml.dao.DAOUtilitaire.fermeturesSilencieuses;
import static com.ml.dao.DAOUtilitaire.initialisationRequetePreparee;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.ml.beans.Artist;
import com.ml.beans.Movie;
import com.ml.beans.Utilisateur;

public class MovieDaoImpl implements MovieDao {

    private DAOFactory          daoFactory;

    private static final String SQL_SELECT_MOVIE           = "SELECT M.movieid, M.title, M.year, R.rank, G.genre "
                                                                   + "FROM movies M, ratings R, genres G "
                                                                   + "WHERE LOWER(M.title) LIKE ? "
                                                                   + "AND M.movieid = R.movieid "
                                                                   + "AND G.movieid = M.movieid "
                                                                   + "GROUP BY M.movieid "
                                                                   + "ORDER BY R.votes DESC "
                                                                   + "LIMIT 5";

    private static final String SQL_SELECT_MOVIE_BY_ID     = "SELECT M.movieid, M.title, M.year, M.imdbid, M.poster, R.rank, R.votes, G.genre "
                                                                   + "FROM movies M, ratings R, genres G "
                                                                   + "WHERE M.movieid = ? "
                                                                   + "AND M.movieid = R.movieid "
                                                                   + "AND M.movieid = G.movieid";

    private static final String SQL_SELECT_ACTORS_MOVIE    = "SELECT A.actorid, A.name "
                                                                   + "FROM actors A, movies2actors M "
                                                                   + "WHERE M.movieid = ? "
                                                                   + "AND A.actorid = M.actorid";

    private static final String SQL_SELECT_DIRECTORS_MOVIE = "SELECT D.directorid, D.name "
                                                                   + "FROM directors D, movies2directors M "
                                                                   + "WHERE M.movieid = ? "
                                                                   + "AND D.directorid = M.directorid";

    private static final String SQL_SELECT_DURATION_MOVIE  = "SELECT time FROM runningtimes WHERE movieid = ?";

    private static final String SQL_INSERT_MOVIE           = "INSERT INTO movies2users VALUES(?, ?)";
    private static final String SQL_REMOVE_MOVIE           = "DELETE FROM movies2users WHERE movieid = ? AND userid = ?";

    MovieDaoImpl( DAOFactory daoFactory ) {
        this.daoFactory = daoFactory;
    }

    @Override
    public ArrayList<Movie> trouver( String title, Utilisateur utilisateur ) throws DAOException {

        Connection connexion = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        ArrayList<Movie> movies = new ArrayList<Movie>();
        Movie movie = null;
        int nbMovies = 1;

        try {
            /* Récupération d'une connexion depuis la Factory */

            connexion = daoFactory.getConnection();
            preparedStatement = initialisationRequetePreparee( connexion, SQL_SELECT_MOVIE, false, true,
                    title.toLowerCase() );
            resultSet = preparedStatement.executeQuery();
            /*
             * Parcours de la ligne de données des éventuels ResulSet retourné
             */

            while ( resultSet.next() && nbMovies <= 10 ) {

                movie = map( resultSet, utilisateur );

                movies.add( movie );
                nbMovies++;

            }
        } catch ( SQLException e ) {
            throw new DAOException( e );
        } finally {
            fermeturesSilencieuses( resultSet, preparedStatement, connexion );
        }

        return movies;
    }

    public Movie display( long idMovie ) {

        Movie movie = fillDataMovie( idMovie );
        movie.setActors( fillActorsMovie( idMovie ) );
        movie.setDirector( fillDirectorsMovie( idMovie ) );
        movie.setDuration( fillDurationMovie( idMovie ) );

        return movie;
    }

    public Movie fillDataMovie( long idMovie ) {

        Connection connexion = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Movie movie = null;

        try {
            /* Récupération d'une connexion depuis la Factory */

            connexion = daoFactory.getConnection();
            preparedStatement = initialisationRequetePreparee( connexion, SQL_SELECT_MOVIE_BY_ID, false, false,
                    idMovie );
            resultSet = preparedStatement.executeQuery();

            if ( resultSet.next() ) {
                movie = mapDataMovie( resultSet );
            }
        } catch ( SQLException e ) {
            throw new DAOException( e );
        } finally {
            fermeturesSilencieuses( resultSet, preparedStatement, connexion );
        }

        return movie;
    }

    public ArrayList<Artist> fillActorsMovie( long idMovie ) {

        Connection connexion = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        ArrayList<Artist> actors;

        try {
            /* Récupération d'une connexion depuis la Factory */

            connexion = daoFactory.getConnection();
            preparedStatement = initialisationRequetePreparee( connexion, SQL_SELECT_ACTORS_MOVIE, false, false,
                    idMovie );
            resultSet = preparedStatement.executeQuery();

            actors = mapActorsMovie( resultSet );

        } catch ( SQLException e ) {
            throw new DAOException( e );
        } finally {
            fermeturesSilencieuses( resultSet, preparedStatement, connexion );
        }

        return actors;
    }

    public Artist fillDirectorsMovie( long idMovie ) {

        Connection connexion = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Artist director;

        try {
            /* Récupération d'une connexion depuis la Factory */

            connexion = daoFactory.getConnection();
            preparedStatement = initialisationRequetePreparee( connexion, SQL_SELECT_DIRECTORS_MOVIE, false, false,
                    idMovie );
            resultSet = preparedStatement.executeQuery();

            director = mapDirectorsMovie( resultSet );

        } catch ( SQLException e ) {
            throw new DAOException( e );
        } finally {
            fermeturesSilencieuses( resultSet, preparedStatement, connexion );
        }

        return director;
    }

    private int fillDurationMovie( long idMovie ) {

        Connection connexion = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        int duration = 0;

        try {
            /* Récupération d'une connexion depuis la Factory */

            connexion = daoFactory.getConnection();
            preparedStatement = initialisationRequetePreparee( connexion, SQL_SELECT_DURATION_MOVIE, false, false,
                    idMovie );
            resultSet = preparedStatement.executeQuery();

            if ( resultSet.next() ) {
                duration = resultSet.getInt( "time" );
            }

        } catch ( SQLException e ) {
            throw new DAOException( e );
        } finally {
            fermeturesSilencieuses( resultSet, preparedStatement, connexion );
        }

        return duration;
    }

    @Override
    public void insert( long idMovie, long idUser ) {

        Connection connexion = null;
        PreparedStatement preparedStatement = null;

        try {
            /* Récupération d'une connexion depuis la Factory */
            connexion = daoFactory.getConnection();
            preparedStatement = initialisationRequetePreparee( connexion, SQL_INSERT_MOVIE, false, false,
                    idMovie, idUser );
            int statut = preparedStatement.executeUpdate();
            /* Analyse du statut retourné par la requête d'insertion */
            if ( statut == 0 ) {
                throw new DAOException( "Échec de l'insertion d'un nouveau film dans la base." );
            }

        } catch ( SQLException e ) {
            throw new DAOException( e );
        } finally {
            fermeturesSilencieuses( preparedStatement, connexion );
        }
    }

    @Override
    public void delete( long idMovie, long idUser ) {

        Connection connexion = null;
        PreparedStatement preparedStatement = null;

        try {
            /* Récupération d'une connexion depuis la Factory */
            connexion = daoFactory.getConnection();
            preparedStatement = initialisationRequetePreparee( connexion, SQL_REMOVE_MOVIE, false, false,
                    idMovie, idUser );
            int statut = preparedStatement.executeUpdate();
            /* Analyse du statut retourné par la requête d'insertion */
            if ( statut == 0 ) {
                throw new DAOException( "Échec de la suppression du film dans la base." );
            }

        } catch ( SQLException e ) {
            throw new DAOException( e );
        } finally {
            fermeturesSilencieuses( preparedStatement, connexion );
        }
    }

    /* 2 - Mapping d'un ResultSet dans un bean */
    private static Movie map( ResultSet resultSet, Utilisateur utilisateur ) throws SQLException {

        Movie movie = new Movie();
        movie.setId( resultSet.getLong( 1 ) );
        movie.setTitle( resultSet.getString( 2 ) );
        movie.setYear( Integer.parseInt( resultSet.getString( 3 ) ) );
        movie.setRate( resultSet.getString( 4 ) );
        movie.setGenre( resultSet.getString( 5 ) );

        if ( utilisateur != null )
            movie.setWatched( utilisateur.getMoviesList().contains( resultSet.getLong( 1 ) ) );

        return movie;
    }

    private static Movie mapDataMovie( ResultSet resultSet ) throws SQLException {

        Movie movie = new Movie();

        try {

            movie.setId( resultSet.getLong( 1 ) );
            movie.setTitle( resultSet.getString( 2 ) );
            movie.setYear( Integer.parseInt( resultSet.getString( 3 ) ) );
            movie.setImdbId( resultSet.getString( 4 ) );
            movie.setPoster( resultSet.getString( 5 ) );
            movie.setRate( resultSet.getString( 6 ) );
            movie.setVotes( resultSet.getInt( 7 ) );
            movie.setGenre( resultSet.getString( 8 ) );
        } catch ( Exception e ) {

        }

        return movie;
    }

    private static ArrayList<Artist> mapActorsMovie( ResultSet resultSet ) throws SQLException {

        ArrayList<Artist> actors = new ArrayList<Artist>();

        while ( resultSet.next() ) {

            try {
                Artist actor = new Artist();

                actor.setIdArtist( resultSet.getLong( 1 ) );
                actor.setLastName(
                        resultSet.getString( 2 ).substring( 0, resultSet.getString( 2 ).indexOf( "," ) ).trim() );

                if ( resultSet.getString( 2 ).indexOf( "(" ) != -1 ) {
                    actor.setFirstName(
                            resultSet.getString( 2 ).substring( resultSet.getString( 2 ).indexOf( "," ) + 1,
                                    resultSet.getString( 2 ).indexOf( "(" ) ) );
                } else {
                    actor.setFirstName(
                            resultSet.getString( 2 ).substring( resultSet.getString( 2 ).indexOf( "," ) + 1 ) );
                }

                actors.add( actor );
            } catch ( Exception e ) {

            }
        }

        return actors;
    }

    private Artist mapDirectorsMovie( ResultSet resultSet ) throws SQLException {

        Artist director = null;

        if ( resultSet.next() ) {

            try {
                director = new Artist();

                director.setIdArtist( resultSet.getLong( 1 ) );
                director.setLastName(
                        resultSet.getString( 2 ).substring( 0, resultSet.getString( 2 ).indexOf( "," ) ).trim() );
                if ( resultSet.getString( 2 ).indexOf( "(" ) != -1 ) {
                    director.setFirstName(
                            resultSet.getString( 2 ).substring( resultSet.getString( 2 ).indexOf( "," ) + 1,
                                    resultSet.getString( 2 ).indexOf( "(" ) ) );
                } else {
                    director.setFirstName(
                            resultSet.getString( 2 ).substring( resultSet.getString( 2 ).indexOf( "," ) + 1 ) );
                }
            } catch ( Exception e ) {

            }
        }

        return director;
    }
}
