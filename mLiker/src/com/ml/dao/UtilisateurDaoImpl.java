package com.ml.dao;

import static com.ml.dao.DAOUtilitaire.fermeturesSilencieuses;
import static com.ml.dao.DAOUtilitaire.initialisationRequetePreparee;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.ml.beans.Utilisateur;

public class UtilisateurDaoImpl implements UtilisateurDao {

    private DAOFactory          daoFactory;
    private static final String SQL_SELECT_PAR_EMAIL  = "SELECT id, email, pseudo, password FROM users WHERE email = ?";
    private static final String SQL_SELECT_PAR_PSEUDO = "SELECT id, email, pseudo, password FROM users WHERE pseudo = ?";
    private static final String SQL_SELECT            = "SELECT id, email, pseudo, password FROM users WHERE email = ? AND password = ?";
    private static final String SQL_SELECT_MOVIES     = "SELECT movieid FROM movies2users WHERE userid = ?";
    private static final String SQL_INSERT            = "INSERT INTO users (email, password, pseudo, register_date) VALUES (?, ?, ?, NOW())";

    public UtilisateurDaoImpl( DAOFactory daoFactory ) {
        this.daoFactory = daoFactory;
    }

    @Override
    public Utilisateur trouver( String email, String password ) throws DAOException {
        Connection connexion = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Utilisateur utilisateur = null;
        ArrayList<Long> moviesList = new ArrayList<Long>();

        try {
            /* Récupération d'une connexion depuis la Factory */
            connexion = daoFactory.getConnection();
            preparedStatement = initialisationRequetePreparee( connexion, SQL_SELECT, true, false,
                    email, password );
            resultSet = preparedStatement.executeQuery();
            /*
             * Parcours de la ligne de données de l'éventuel ResulSet retourné
             */
            if ( resultSet.next() ) {
                utilisateur = map( resultSet );
            }

            if ( utilisateur != null ) {

                resultSet.close();
                preparedStatement.close();

                preparedStatement = initialisationRequetePreparee( connexion, SQL_SELECT_MOVIES, false, false,
                        utilisateur.getId() );
                resultSet = preparedStatement.executeQuery();

                while ( resultSet.next() ) {

                    moviesList.add( resultSet.getLong( "movieid" ) );
                }

                utilisateur.setMoviesList( moviesList );
            }
        } catch ( SQLException e ) {
            throw new DAOException( e );
        } finally {
            fermeturesSilencieuses( resultSet, preparedStatement, connexion );
        }

        return utilisateur;
    }

    public Utilisateur trouverPseudo( String pseudo ) throws DAOException {
        Connection connexion = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Utilisateur utilisateur = null;

        try {
            /* Récupération d'une connexion depuis la Factory */
            connexion = daoFactory.getConnection();
            preparedStatement = initialisationRequetePreparee( connexion, SQL_SELECT_PAR_PSEUDO, false, false,
                    pseudo );
            resultSet = preparedStatement.executeQuery();
            /*
             * Parcours de la ligne de données de l'éventuel ResulSet retourné
             */
            if ( resultSet.next() ) {
                utilisateur = map( resultSet );
            }
        } catch ( SQLException e ) {
            throw new DAOException( e );
        } finally {
            fermeturesSilencieuses( resultSet, preparedStatement, connexion );
        }

        return utilisateur;
    }

    @Override
    public Utilisateur trouverMail( String email ) throws DAOException {
        Connection connexion = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        Utilisateur utilisateur = null;

        try {
            /* Récupération d'une connexion depuis la Factory */
            connexion = daoFactory.getConnection();
            preparedStatement = initialisationRequetePreparee( connexion, SQL_SELECT_PAR_EMAIL, false, false,
                    email );
            resultSet = preparedStatement.executeQuery();
            /*
             * Parcours de la ligne de données de l'éventuel ResulSet retourné
             */
            if ( resultSet.next() ) {
                utilisateur = map( resultSet );
            }
        } catch ( SQLException e ) {
            throw new DAOException( e );
        } finally {
            fermeturesSilencieuses( resultSet, preparedStatement, connexion );
        }

        return utilisateur;
    }

    @Override
    public void creer( Utilisateur utilisateur ) throws DAOException {
        Connection connexion = null;
        PreparedStatement preparedStatement = null;
        ResultSet valeursAutoGenerees = null;

        try {
            /* Récupération d'une connexion depuis la Factory */
            connexion = daoFactory.getConnection();
            preparedStatement = initialisationRequetePreparee( connexion, SQL_INSERT, true, false,
                    utilisateur.getEmail(), utilisateur.getPassword(), utilisateur.getPseudo() );
            int statut = preparedStatement.executeUpdate();
            /* Analyse du statut retourné par la requête d'insertion */
            if ( statut == 0 ) {
                throw new DAOException( "Échec de la création de l'utilisateur, aucune ligne ajoutée dans la table." );
            }
            /* Récupération de l'id auto-généré par la requête d'insertion */
            valeursAutoGenerees = preparedStatement.getGeneratedKeys();
            if ( valeursAutoGenerees.next() ) {

                utilisateur.setId( valeursAutoGenerees.getLong( 1 ) );
            } else {
                throw new DAOException(
                        "Échec de la création de l'utilisateur en base, aucun ID auto-généré retourné." );
            }
        } catch ( SQLException e ) {
            throw new DAOException( e );
        } finally {
            fermeturesSilencieuses( valeursAutoGenerees, preparedStatement, connexion );
        }
    }

    /* 2 - Mapping d'un ResultSet dans un bean */
    private static Utilisateur map( ResultSet resultSet ) throws SQLException {
        Utilisateur utilisateur = new Utilisateur();
        utilisateur.setId( resultSet.getLong( "id" ) );
        utilisateur.setEmail( resultSet.getString( "email" ) );
        utilisateur.setPassword( resultSet.getString( "password" ) );
        utilisateur.setPseudo( resultSet.getString( "pseudo" ) );

        return utilisateur;
    }
}