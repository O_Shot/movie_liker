package com.ml.dao;

public class DAOException extends RuntimeException {

    private static final long serialVersionUID = 3638926499035063712L;

    public DAOException( String message ) {
        super( message );
    }

    public DAOException( String message, Throwable cause ) {
        super( message, cause );
    }

    public DAOException( Throwable cause ) {
        super( cause );
    }
}