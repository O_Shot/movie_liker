package com.ml.dao;

import java.util.ArrayList;

import com.ml.beans.Movie;
import com.ml.beans.Utilisateur;

public interface MovieDao {

    ArrayList<Movie> trouver( String title, Utilisateur utilisateur ) throws DAOException;

    Movie display( long idMovie );

    void insert( long idMovie, long idUser );

    void delete( long idMovie, long idUser );

}
