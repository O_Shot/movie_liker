package com.ml.validators;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import com.ml.service.UsersService;

@ManagedBean
@RequestScoped
public class ExistenceEmailValidator implements Validator {

    private static final String EMAIL_EXISTE_DEJA = "Cette adresse email est déjà utilisée";
    private static final String VALID_EMAIL       = "Merci de saisir une adresse mail valide.";
    private static final String OVER_LENGTH_EMAIL = "L'adresse mail ne doit pas contenir plus de 100 caractères";
    private static final String EMPTY             = "Merci de saisir une adresse mail";

    @ManagedProperty( "#{usersService}" )
    private UsersService        usersService;

    @Override
    public void validate( FacesContext context, UIComponent component, Object value ) throws ValidatorException {

        String email = (String) value;

        if ( email != null ) {
            if ( !email.matches( "([^.@]+)(\\.[^.@]+)*@([^.@]+\\.)+([^.@]+)" ) ) {
                throw new ValidatorException( new FacesMessage( FacesMessage.SEVERITY_ERROR, VALID_EMAIL, null ) );
            } else if ( email.length() > 100 ) {
                throw new ValidatorException(
                        new FacesMessage( FacesMessage.SEVERITY_ERROR, OVER_LENGTH_EMAIL, null ) );
            }
        } else {
            throw new ValidatorException( new FacesMessage( FacesMessage.SEVERITY_ERROR, EMPTY, null ) );
        }

        if ( usersService.existingMail( email ) ) {
            throw new ValidatorException(
                    new FacesMessage( FacesMessage.SEVERITY_ERROR, EMAIL_EXISTE_DEJA, null ) );
        }
    }

    public UsersService getUsersService() {
        return usersService;
    }

    public void setUsersService( UsersService usersService ) {
        this.usersService = usersService;
    }

}
