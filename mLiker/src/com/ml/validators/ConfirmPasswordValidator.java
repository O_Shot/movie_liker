package com.ml.validators;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

@FacesValidator( value = "confirmPasswordValidator" )
public class ConfirmPasswordValidator implements Validator {

    private static final String FIELD_PASSWORD = "componentPassword";
    private static final String ERROR_PASSWORD = "Le mot de passe et la confirmation doivent être identiques.";
    private static final String SHORT_PASS     = "Le mot de passe doit contenir au moins 3 caractères.";
    private static final String LONG_PASS      = "Le mot de passe ne doit pas contenir plus de 50 caractères.";
    private static final String EMPTY_PASS     = "Merci de saisir votre mot de passe.";

    @Override
    public void validate( FacesContext context, UIComponent component, Object value ) throws ValidatorException {

        UIInput composantMotDePasse = (UIInput) component.getAttributes().get( FIELD_PASSWORD );

        String password = (String) composantMotDePasse.getValue();
        String confirmed = (String) value;

        if ( password != null ) {
            if ( password.length() < 3 ) {
                throw new ValidatorException( new FacesMessage( FacesMessage.SEVERITY_ERROR, SHORT_PASS, null ) );
            } else if ( password.length() > 50 ) {
                throw new ValidatorException( new FacesMessage( FacesMessage.SEVERITY_ERROR, LONG_PASS, null ) );
            } else if ( confirmed != null && !confirmed.equals( password ) ) {
                throw new ValidatorException( new FacesMessage( FacesMessage.SEVERITY_ERROR, ERROR_PASSWORD, null ) );
            }
        } else {
            throw new ValidatorException( new FacesMessage( FacesMessage.SEVERITY_ERROR, EMPTY_PASS, null ) );
        }

    }
}