package com.ml.validators;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedProperty;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import com.ml.service.UsersService;

@FacesValidator( value = "pseudoValidator" )
public class PseudoValidator implements Validator {

    private final static String SHORT_PSEUDO = "Le pseudo doit contenir au moins 2 caractères.";
    private final static String LONG_PSEUDO  = "Le pseudo ne doit pas contenir plus de 20 caractères.";
    private final static String EMPTY_PSEUDO = "Merci de saisir votre pseudo";

    @ManagedProperty( "#{usersService}" )
    private UsersService        usersService;

    @Override
    public void validate( FacesContext context, UIComponent component, Object value ) throws ValidatorException {

        String pseudo = (String) value;

        if ( pseudo != null ) {
            if ( pseudo.length() < 2 ) {
                throw new ValidatorException( new FacesMessage( FacesMessage.SEVERITY_ERROR, SHORT_PSEUDO, null ) );
            } else if ( pseudo.length() > 50 ) {
                throw new ValidatorException( new FacesMessage( FacesMessage.SEVERITY_ERROR, LONG_PSEUDO, null ) );
            }
        } else {
            throw new ValidatorException( new FacesMessage( FacesMessage.SEVERITY_ERROR, EMPTY_PSEUDO, null ) );
        }
    }

    public UsersService getUsersService() {
        return usersService;
    }

    public void setUsersService( UsersService usersService ) {
        this.usersService = usersService;
    }

}
