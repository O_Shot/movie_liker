package com.ml.model;
// Generated 13 f�vr. 2016 17:52:31 by Hibernate Tools 4.3.1.Final

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * QuotesId generated by hbm2java
 */
@Embeddable
public class QuotesId implements java.io.Serializable {

    private int    movieid;
    private String quotetext;

    public QuotesId() {
    }

    public QuotesId( int movieid ) {
        this.movieid = movieid;
    }

    public QuotesId( int movieid, String quotetext ) {
        this.movieid = movieid;
        this.quotetext = quotetext;
    }

    @Column( name = "movieid", nullable = false )
    public int getMovieid() {
        return this.movieid;
    }

    public void setMovieid( int movieid ) {
        this.movieid = movieid;
    }

    @Column( name = "quotetext", length = 16777215 )
    public String getQuotetext() {
        return this.quotetext;
    }

    public void setQuotetext( String quotetext ) {
        this.quotetext = quotetext;
    }

    public boolean equals( Object other ) {
        if ( ( this == other ) )
            return true;
        if ( ( other == null ) )
            return false;
        if ( !( other instanceof QuotesId ) )
            return false;
        QuotesId castOther = (QuotesId) other;

        return ( this.getMovieid() == castOther.getMovieid() )
                && ( ( this.getQuotetext() == castOther.getQuotetext() )
                        || ( this.getQuotetext() != null && castOther.getQuotetext() != null
                                && this.getQuotetext().equals( castOther.getQuotetext() ) ) );
    }

    public int hashCode() {
        int result = 17;

        result = 37 * result + this.getMovieid();
        result = 37 * result + ( getQuotetext() == null ? 0 : this.getQuotetext().hashCode() );
        return result;
    }

}
