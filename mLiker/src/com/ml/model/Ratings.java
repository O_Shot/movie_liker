package com.ml.model;
// Generated 13 f�vr. 2016 17:52:31 by Hibernate Tools 4.3.1.Final

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Ratings generated by hbm2java
 */
@Entity
@Table( name = "ratings", catalog = "jmdb" )
public class Ratings implements java.io.Serializable {

    private RatingsId id;

    public Ratings() {
    }

    public Ratings( RatingsId id ) {
        this.id = id;
    }

    @EmbeddedId

    @AttributeOverrides( {
            @AttributeOverride( name = "movieid", column = @Column( name = "movieid", nullable = false ) ),
            @AttributeOverride( name = "rank", column = @Column( name = "rank", nullable = false, length = 4 ) ),
            @AttributeOverride( name = "votes", column = @Column( name = "votes" ) ),
            @AttributeOverride( name = "distribution", column = @Column( name = "distribution", nullable = false, length = 10 ) ) })
    public RatingsId getId() {
        return this.id;
    }

    public void setId( RatingsId id ) {
        this.id = id;
    }

}
