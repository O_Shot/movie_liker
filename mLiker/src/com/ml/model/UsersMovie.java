package com.ml.model;

public class UsersMovie {

    private Movies  movie;
    private Integer rate;

    public UsersMovie() {

    }

    public UsersMovie( Movies movies, Integer rate ) {
        this.movie = movies;
        this.rate = rate;
    }

    public Movies getMovie() {
        return movie;
    }

    public void setMovie( Movies movie ) {
        this.movie = movie;
    }

    public Integer getRate() {
        return rate;
    }

    public void setRate( Integer rate ) {
        this.rate = rate;
    }

}
