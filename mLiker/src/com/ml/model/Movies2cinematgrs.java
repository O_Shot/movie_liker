package com.ml.model;
// Generated 13 f�vr. 2016 17:52:31 by Hibernate Tools 4.3.1.Final

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Movies2cinematgrs generated by hbm2java
 */
@Entity
@Table( name = "movies2cinematgrs", catalog = "jmdb" )
public class Movies2cinematgrs implements java.io.Serializable {

    private Movies2cinematgrsId id;

    public Movies2cinematgrs() {
    }

    public Movies2cinematgrs( Movies2cinematgrsId id ) {
        this.id = id;
    }

    @EmbeddedId

    @AttributeOverrides( {
            @AttributeOverride( name = "movieid", column = @Column( name = "movieid", nullable = false ) ),
            @AttributeOverride( name = "cinematid", column = @Column( name = "cinematid", nullable = false ) ),
            @AttributeOverride( name = "addition", column = @Column( name = "addition", length = 1000 ) ) })
    public Movies2cinematgrsId getId() {
        return this.id;
    }

    public void setId( Movies2cinematgrsId id ) {
        this.id = id;
    }

}
