package com.ml.beans;

import java.util.ArrayList;

public class Utilisateur {

    private Long            id;
    private String          pseudo;
    private String          email;
    private String          password;
    private ArrayList<Long> moviesList;

    public ArrayList<Long> getMoviesList() {
        return moviesList;
    }

    public void setMoviesList( ArrayList<Long> moviesList ) {
        this.moviesList = moviesList;
    }

    public Long getId() {
        return id;
    }

    public void setId( Long id ) {
        this.id = id;
    }

    public String getPseudo() {
        return pseudo;
    }

    public void setPseudo( String pseudo ) {
        this.pseudo = pseudo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail( String email ) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword( String password ) {
        this.password = password;
    }

}
