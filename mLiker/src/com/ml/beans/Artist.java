package com.ml.beans;

public class Artist {

    private long   idArtist;
    private String firstName;
    private String lastName;

    public long getIdArtist() {
        return idArtist;
    }

    public void setIdArtist( long idArtist ) {
        this.idArtist = idArtist;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName( String firstName ) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName( String lastName ) {
        this.lastName = lastName;
    }

}
