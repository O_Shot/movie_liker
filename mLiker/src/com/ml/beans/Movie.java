package com.ml.beans;

import java.util.ArrayList;

public class Movie {

    private long              id;
    private String            title;
    private int               year;
    private String            imdbId;
    private String            poster;
    private Artist            director;
    private ArrayList<Artist> actors;
    private String            genre;
    private String            rate;
    private int               votes;
    private int               duration;
    private boolean           watched;

    public boolean isWatched() {
        return watched;
    }

    public void setWatched( boolean watched ) {
        this.watched = watched;
    }

    public long getId() {
        return id;
    }

    public void setId( long id ) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle( String title ) {
        this.title = title;
    }

    public int getYear() {
        return year;
    }

    public void setYear( int year ) {
        this.year = year;
    }

    public String getImdbId() {
        return imdbId;
    }

    public void setImdbId( String imdbId ) {
        this.imdbId = imdbId;
    }

    public String getPoster() {
        return poster;
    }

    public void setPoster( String poster ) {
        this.poster = poster;
    }

    public Artist getDirector() {
        return director;
    }

    public void setDirector( Artist director ) {
        this.director = director;
    }

    public ArrayList<Artist> getActors() {
        return actors;
    }

    public void setActors( ArrayList<Artist> actors ) {
        this.actors = actors;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre( String genre ) {
        this.genre = genre;
    }

    public String getRate() {
        return rate;
    }

    public void setRate( String rate ) {
        this.rate = rate;
    }

    public int getVotes() {
        return votes;
    }

    public void setVotes( int votes ) {
        this.votes = votes;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration( int duration ) {
        this.duration = duration;
    }

}
