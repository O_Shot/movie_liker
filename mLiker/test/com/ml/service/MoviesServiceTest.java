package com.ml.service;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MoviesServiceTest {

    private static ClassPathXmlApplicationContext context;
    // private static MoviesService moviesService;

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        context = new ClassPathXmlApplicationContext( "application-context.xml" );
        // moviesService = (MoviesService) context.getBean( "moviesService" );
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        context.close();
    }

    /*
     * @Test public void testGetByTitle() { List<Movies> movies =
     * moviesService.getByTitle( "Titanic" ); assertNotNull( movies );
     * assertTrue( movies.size() > 0 ); }
     * 
     * @Test public void testGetById() { Movies movie = moviesService.getById(
     * 3502914 ); assertNotNull( movie ); assertEquals( Integer.valueOf( 3502914
     * ), movie.getMovieid() ); System.out.println( movie.getImdbid() ); }
     */

}
